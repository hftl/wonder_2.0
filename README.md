WONDER 2.0
======

The WONDER javascript library is the Reference Implementation of the [Signalling On-the-Fly](https://github.com/hypercomm/wonder/wiki/Signalling-on-the-fly) concept enabling seamless interoperability between different WebRTC Service Provider domains.

# Setup
## Dependencies on Debian
* copy these lines in the shell:
```
sudo apt-get install nodejs curl                # nodejs and curl
sudo apt-get install nodejs-legacy              # symlinks from nodejs to node for using nodejs on debian
curl -L https://npmjs.org/install.sh | sudo sh  # npm packet manager
```

## Wonder Framework
* copy the following commands in the shell before starting to work on or with wonder
```
sudo npm install -g gulp    # install the nodejs task runner gulp
npm install                 # install all dependencies
```

## Configuring NodeJS-domain
* two different idp's are available
    * webfinger - uses the webfinger protocol to find meta data about users on a domain
    * .nodejs domain specific IdP - a very simple idp which just returns users meta data for the `*.nodejs`-domain
* do not use localhost if you want to test it with different devices

### Webfinger IdP Server (prefered)
* adjust the following files with the appropriate ip adress configuration for your environment.
```
src/webfinger/config.js             # configuration for webfinger server
src/webfinger/data/default.json     # default webfinger meta data if a user wasn't found
src/webfinger/data/acct/*.json      # user specific webfinger meta data
```

### .nodejs domain specific IdP
* adjust the following files with the appropriate ip adress configuration for your environment.
```
src/idp/data/nodejs.wonder          # contains meta data for all users using the `*.nodejs`-domain
```
* create files like `alice_at_nodejs.wonder` for user specific data.

## Configuring the testing website apps/min/
* do the following command

```
cp src/apps/min/config.example.js src/apps/min/config.js
```

* adjust the following file with the appropriate `ip` for your environment.

```
src/apps/min/config.js
```

## Configuring the stub-provider src/stub-provider
* do the following command

```
cp src/stub-provider/config.example.js src/stub-provider/config.js
```

* adjust the following file with the appropriate `ip` for your environment.

```
src/stub-provider/config.js
```

## Configuring the data-codec-provider src/data-codec-provider
* do the following command

```
cp src/data-codec-provider/config.example.js src/data-codec-provider/config.js
```

* adjust the following file with the appropriate `ip` for your environment.

```
src/data-codec-provider/config.js
```

## Structure
* `doc`-folder: contains the documentation of this project
  * run `gulp doc` to build the docs from source code and open it in browser
* `src`-folder: contains the source code of the WONDER 2.0 library
* use `gulp help` for an overview of all available gulp tasks

## Gulp Tasks
* use `gulp help` for an overview of all available gulp tasks
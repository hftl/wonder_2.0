# Apache2 Virtual Host files
You need to follow this instruction to for creating an apache2 virtual host which maps the port of your application to standard ports 443/80.

# intstruction
1. copy the file `vhost.example` to your apache2 vhost directory
  * `sudo cp vhost.example /etc/apache2/sites-available/myvhost`
2. adjust the vhost (explanation you will find inside the vhost)
  * `sudo nano /etc/apache2/sites-available/myvhost`
3. make vhost available for apache2
  * `sudo a2ensite myvhost`
4. restart your apache webserver
  * `sudo service apache2 restart`

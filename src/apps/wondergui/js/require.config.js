require.config({
  baseUrl: '../../../../dist',
  paths: {
    'app': '../apps/wondergui/js/app',
    'gui': '../apps/wondergui/js/gui',
    'config': '../apps/wondergui/js/config'
  }
});

/**
 * @desc WebRTC framework to facilitate the development of Applications which seamlessly interoperate with each other
 * This framework is based on @see https://github.com/hypercomm/wonder
 * @author Danny Koppenhagen <mail@d-koppenhagen.de>
 */

define(['wonder', 'gui', 'config'], function(wonder, gui, config) {
  wonder.config = config.wonder;

  /**
   * global variables
   */
  var conversation = null;
  var filesToSend = null;
  var mute = {
    state: false,
    before: 1 // to save the value before mute
  }

  /**
   * get buttons
   */
  var loginbtn = document.getElementById('loginButton');
  var imsloginBtn = document.getElementById('imsloginBtn');
  var logoutbtn = document.getElementById('logoutBtn');
  var videoBtn = document.getElementById('newVideoCallBtn');
  var audioBtn = document.getElementById('newAudioCallBtn');
  var dataBtn = document.getElementById('openChatBtn');
  var msgBtn = document.getElementById('sendMessage');

  /**
   * get inputs
   */
  var loginname = document.getElementById('loginText');
  var recipient = document.getElementById('callTo');
  var msgInput = document.getElementById('sendMessage');
  var fileInput = document.getElementById('files');

  /**
   * get video elements
   */
  var localVideo = document.getElementById('localVideo');
  var remoteVideo = document.getElementById('remoteVideo');

  /**
   * get textbox
   */
  var textBox = document.getElementById('textChat');

  var fileQueue = []; // queue for files
  var msgQueue = []; // queue for chat messages
  var fileDataChannelEstablished = false;
  var chatDataChannelEstablished = false;

  /**
   * select the right domain (nodejs/ims)
   */
  $("#selectDomain li a").click(function() {
    var domain = $(this).attr("domain");
    console.log('selected Domain: ' + domain);

    switch (domain) {
      case 'nodejs':
        var nodeLogin = loginCred.data;
        // if no entry found in localstorage
        if (nodeLogin == null || nodeLogin == '') {
          $('.nodejsLoginForm').removeClass('hidden'); // show login input
        } else {
          login(nodeLogin, '');
        }
        break;
      case 'ims':
        var imsCred = localStorage.getItem("imsLoginCredentials");
        // hide domain select
        $('.nodejsLoginForm').addClass('hidden');
        // if no localstorage entry found
        if ((imsCred == null) || imsCred == "") {
          $('#modalIMS').modal(); // open IMS login modal
        }
        break;
      default:
        break;
    }
  });


  /**
   * nodejs login
   */
  loginbtn.onclick = function() {
    // check input an save to localstorage
    if (!gui.states.loggedIn && (inputCheck('#loginText') === true)) {
      // get login data
      var user = $('#loginText').val();
      loginCred.setData(user);
    }
    login(loginname.value, '');
    return false;
  }

  if (imsloginBtn) {
    imsloginBtn.onclick = function() {
      var IMSprivateID = document.getElementById('imsPrivateId').value;
      var IMSpublicID = document.getElementById('imsPublicId').value;
      var IMSrealmString = IMSprivateID.split("@");
      var IMSpassword = document.getElementById('imsPass').value;
      var IMSproxy = document.getElementById('imsProxy').value;

      var credentials = {};
      credentials.user = IMSprivateID;
      credentials.pubID = 'sip:' + IMSpublicID + '@' + IMSrealmString[1];
      credentials.role = '';
      credentials.pass = IMSpassword;
      credentials.realm = IMSrealmString[1];
      credentials.pcscf = IMSproxy;

      console.log(credentials);
      login(IMSprivateID, credentials);
      loginCred.setData(user);
      return false;
    }
  }

  /**
   * general login method
   */
  function login(name, credentials) {
    console.log('login...', name, credentials);

    // login only with username
    wonder.login(name, credentials)
      .then(function(data) {
        $.bootstrapGrowl('Successfully signed in!', {
          type: 'success',
          offset: {
            from: 'bottom',
            amount: 60
          }
        });
        $('#status').append(name);
        gui.hide.login();
      })
      .catch(function(error) {
        console.log(error);
        $.bootstrapGrowl('Error:' + error, {
          type: 'error',
          offset: {
            from: 'bottom',
            amount: 60
          }
        });
      });
  }



  /**
   * logout
   */
  logoutbtn.onclick = function() {
    console.log('logout...');

    // login only with username
    wonder.logout()
      .then(function(data) {
        loginCred.removeData;
        contact.removeAll();
        gui.hide.all();
        return false;
      })
      .catch(function(error) {
        console.log(error);
      });
  }



  /**
   * hangup
   */
  $(".remote_container .hangupBtn").click(function() {
    wonder.hangup().then(function() {
      gui.hide.remote();
      $.bootstrapGrowl('Bye!', {
        type: 'success',
        offset: {
          from: 'bottom',
          amount: 60
        }
      });
    }, function(err) {
      $.bootstrapGrowl('Error:' + error, {
        type: 'error',
        offset: {
          from: 'bottom',
          amount: 60
        }
      });
    });
  });



  /**
   * onMessage listener
   */
  wonder.onMessage = function(msg, conversationId) {
    switch (msg.type) {
      case MessageType.invitation:
        console.log('[main] Message invitation:', msg, conversationId);
        gui.show.receiveInvitationModal(msg.from);
        $('#AcceptButton').click(function() {
          wonder.answerRequest(msg, true).then(function() {
            gui.hide.receiveInvitationModal();
            if(msg.misc.demand.in.data == 'file') fileDataChannelEstablished = true;
            if(msg.misc.demand.in.data == 'chat') {
              chatDataChannelEstablished = true;
              gui.show.chat();
            }
            if(msg.misc.demand.in.video) gui.show.video();
            if(msg.misc.demand.in.audio) gui.show.audio();
          });
        });
        $('#RejectButton').click(function() {
          wonder.answerRequest(msg, false).then(function() {
            gui.hide.receiveInvitationModal();
          });
        });
        break;

      case MessageType.accepted:
        console.log('[main] Message accepted:', msg, conversationId);
        if(msg.misc.demand.in.data == 'file') {
          fileDataChannelEstablished = true;
        }
        if(msg.misc.demand.in.data == 'chat') {
          chatDataChannelEstablished = true;
        }
        gui.hide.sendInvitationModal();
        break;

      case MessageType.declined:
        console.log('[main] Message declined:', msg, conversationId);
        gui.hide.sendInvitationModal();
        break;

      case MessageType.bye:
        console.log('[main] Message: bye', msg, conversationId);
        gui.hide.remoteVideo();
        break;

      case MessageType.update:
        console.log('[main] Message: update', msg, conversationId);
        break;

      case MessageType.updateSdp:
        console.log('[main] Message: updateSdp', msg, conversationId);
        break;

      case MessageType.updated:
        console.log('[main] Message: updated', msg, conversationId);
        break;

      case MessageType.connectivityCandidate:
        console.log('[main] Message: connectivityCandidate', msg, conversationId);
        break;

      case MessageType.message:
        console.log('[main] Message: message', msg, conversationId);
        break;

      default:
        console.log('[main] Message default:', msg, conversationId);
        break;
    }
  }



  /**
   * onRtcEvt listener
   */
  wonder.onRtcEvt = function(evt, conversationId) {
    switch (evt.type) {
      case RtcEvtType.onaddstream:
        console.log('[main] RtcEvt onaddstream:', evt, conversationId);
        attachMediaStream(remoteVideo, evt.stream);
        gui.show.video();
        break;

      case RtcEvtType.onaddlocalstream:
        console.log('[main] RtcEvt onaddlocalstream:', evt, conversationId);
        attachMediaStream(localVideo, evt.stream);
        gui.show.localVideo();
        break;

      case RtcEvtType.onResourceParticipantAddedEvt:
        console.log('[main] RtcEvt onResourceParticipantAddedEvt:', evt, conversationId);
        break;

      case RtcEvtType.onnegotiationneeded:
        console.log('[main] RtcEvt onnegotiationneeded:', evt, conversationId);
        break;

      case RtcEvtType.onicecandidate:
        console.log('[main] RtcEvt onicecandidate:', evt, conversationId);
        break;

      case RtcEvtType.onsignalingstatechange:
        console.log('[main] RtcEvt onsignalingstatechange:', evt, conversationId);
        break;

      case RtcEvtType.onremovestream:
        console.log('[main] RtcEvt onremovestream:', evt, conversationId);
        break;

      case RtcEvtType.oniceconnectionstatechange:
        console.log('[main] RtcEvt oniceconnectionstatechange:', evt, conversationId);
        break;

      case RtcEvtType.ondatachannel:
        console.log('[main] RtcEvt ondatachannel:', evt, conversationId);
        if(fileDataChannelEstablished) sendMessages(fileQueue, PayloadType.file);
        if(chatDataChannelEstablished) sendMessages(msgQueue, PayloadType.chat);

        break;

      default:
        console.log('[main] RtcEvt default:', evt, conversationId);
        break;
    }
  }



  /**
   * onDataChannelEvt listener
   */
  wonder.onDataChannelEvt = function(evt, conversationId) {
    switch (evt.type) {
      case DataChannelEvtType.onopen:
        gui.show.chat();
        console.log('[main] DataChannelEvt onopen:', evt, conversationId);
        break;

      case DataChannelEvtType.onclose:
        console.log('[main] DataChannelEvt onclose:', evt, conversationId);
        break;

      case DataChannelEvtType.ondatachannel:
        console.log('[main] DataChannelEvt ondatachannel:', evt, conversationId);
        break;

      case DataChannelEvtType.onmessage:
        console.log('[main] DataChannelEvt onmessage:', evt, conversationId);
        gui.show.chat();

        var parsedData = JSON.parse(evt.data)
        var sender = parsedData.from;
        var msgData = parsedData.content;

        // append received text to textbox
        gui.show.chat();
        var iDiv = document.getElementById('textChat');

        var innerDiv = document.createElement('li');
        innerDiv.className = 'list-group-item block-2';

        iDiv.appendChild(innerDiv);

        innerDiv.innerHTML = '<span class="label label-default">' + sender + "</span>" + " <span>" + msgData + "</span>";

        $("#textChat").animate({
          scrollTop: $("#textChat").height()
        }, 1000);
        break;

      default:
        console.log('[main] DataChannelEvt default:', evt, conversationId);
        break;
    }
  }



  /**
   * start a video call
   */
  videoBtn.onclick = function() {
    console.log('start video call...');
    gui.show.sendInvitationModal();

    wonder.call(recipient.value, {audio: true, video: true}).then(function() {
      gui.hide.sendInvitationModal();
    });
  }



  /**
   * start an audio call
   */
  audioBtn.onclick = function() {
    console.log('start audio call...');
    gui.show.sendInvitationModal();

    wonder.call(recipient.value, {audio: true}).then(function() {
      gui.hide.sendInvitationModal();

    });
  }



  /**
   * start a chat
   */
  dataBtn.onclick = function() {
    console.log('start chat...');
    wonder.call(recipient.value, {data: 'chat'}).then(function() {
      gui.show.chat();
    });
  }



  /**
   * data channel operations
   */
  msgBtn.onclick = function() {
    var messageText = $('.summernote').code();
    console.log('message to send: ', messageText);

    var msg = {
      from: loginname.value,
      to: recipient.value,
      content: messageText
    }

    msgQueue.push(msg); //push msg to queue

    if(!chatDataChannelEstablished) {
      wonder.call(recipient.value, {data: 'chat'});
    } else { // wait unti accepted
      sendMessages(msgQueue, PayloadType.chat, function(){
        var iDiv = document.getElementById('textChat');
        // Now create and append to iDiv
        var innerDiv = document.createElement('li');
        innerDiv.className = 'list-group-item block-2';
        iDiv.appendChild(innerDiv);
        innerDiv.innerHTML = '<span class="label label-primary">Me</span>' + ' <span>' + msg.content + '</span>';

        $(".note-editor .note-editable p").empty();
        $(".note-editor textarea").focus();
        $("#textChat").animate({
          scrollTop: $("#textChat").height()
        }, 1000);
      });
    }
  }



  /**
   * send a file
   */
  $('.remoteFileBtn, #fileBtn').click(function(){
    fileInput.click();
  });

  fileInput.onchange = function(evt) {
    console.log('file input changed', this.files);
    filesToSend = this.files; // files as an array

    handleFileSelect(evt, function(){
      console.log('files to send: ', filesToSend);

      var msg = {
        from: loginname.value,
        to: recipient.value,
        files: filesToSend
      }

      fileQueue.push(msg); //push msg to queue

      if(!fileDataChannelEstablished) {
        wonder.call(recipient.value, {data: 'file'});
      } else { // wait unti accepted
        sendMessages(fileQueue, PayloadType.file);
      }
    });
  }
  function handleFileSelect(evt, callback) {
    files = evt.target.files; // FileList object
    // files is a FileList of File objects. List some properties.
    var output = [];
    for (var i = 0, f; f = files[i]; i++) {
      output.push('<span class="label label-default">' + f.name + ' (' + (f.type || 'n/a') + ') - ' + f.size + ' bytes </span>');
    }
    $('#output').append('<div>' + output.join('<br>') + '</div>');
    callback();
  }

  function sendMessages(queue, payloadType, callback){
    if(queue.length > 0){
      wonder.dataChannelMsg(queue[0], payloadType)
      .then(function(){
        queue.splice(0, 1); // delete this element
        if(callback) callback(); // proceed callback if there is one
        sendMessages(queue, payloadType); // send next message
      });
    }
  }


  /**
   * Fullscreen
   */
  $(".remote_container .remoteExpandBtn").click(function() {
    fullscreen(remoteVideo);
  });
  $("#localContainer .localExpandBtn").click(function() {
    fullscreen(localVideo);
  });

  function fullscreen(elem) {
    if (elem.requestFullscreen) {
      elem.requestFullscreen();
    } else if (elem.mozRequestFullScreen) {
      elem.mozRequestFullScreen();
    } else if (elem.webkitRequestFullscreen) {
      elem.webkitRequestFullscreen();
    }
  }



  /**
   * buttons for controlling remote media constraints
   */
  $('.remote_container .remoteChatBtn').click(function() {
    console.log('changing remote constraints: chat', gui.states.chat);
    if (!gui.states.chat) {
      wonder.call(recipient.value, { data: 'chat' }).then(function() {
        gui.show.chat();
      });
    } else {
      wonder.call(recipient.value, { data: false }).then(function() {
        gui.hide.chat();
      });
    }
  });

  $('.remote_container .remoteVideoBtn').click(function() {
    console.log('changing remote constraints: video', gui.states.video.remote);
    if (!gui.states.video.remote) {
      wonder.call(recipient.value, { video: true, audio: true }).then(function() {
        gui.show.av();
      });
    } else {
      wonder.call(recipient.value, { video: false, audio: false }).then(function() {
        gui.hide.remoteVideo();
      });
    }
  });

  $('.remote_container .remoteAudioBtn').click(function() {
    console.log('changing remote constraints: audio', gui.states.audio);
    if (!gui.states.audio) {
      wonder.call(recipient.value, { audio: true }).then(function() {
        gui.show.audio();
      });
    } else {
      wonder.call(recipient.value, { audio: false }).then(function() {
        gui.hide.audio();
      });
    }
  });

  // buttons for controlling local media constraints
  $("#localContainer .localVideoBtn").click(function() {
    if (!gui.states.video.local) {
      wonder.call(recipient.value, {
        out: {
          video: true
        }
      }).then(function() {
        gui.show.localVideo();
      });
    } else {
      wonder.call(recipient.value, {
        out: {
          video: false
        }
      }).then(function() {
        gui.hide.localVideo();
      });
    }
  });

  $("#localContainer .localMicBtn").click(function() {
    if (!gui.states.video.local) {
      wonder.call(recipient.value, {
        out: {
          audio: true
        }
      }).then(function() {
        gui.show.audio();
      });
    } else {
      wonder.call(recipient.value, {
        out: {
          audio: false
        }
      }).then(function() {
        gui.hide.audio();
      });
    }
  });



  /**
   * cancel an invitation
   */
  $("#CancelInviting").click(function() {
    gui.hide.sendInvitationModal();
  });



  /**
   * change volume
   */
  $(".remote_container .remoteVolumeDown").click(function() {
    var elem = document.getElementById("remoteVideo");
    volume(elem, 'down');
    if (mute.state) {
      $(".remote_container .remoteVolumeMute").removeClass('active btn-danger');
      volume(elem, 'unmute');
    }
    volume(elem, 'down');
  });
  $(".remote_container .remoteVolumeUp").click(function() {
    var elem = document.getElementById("remoteVideo");
    if (mute.state) {
      $(".remote_container .remoteVolumeMute").removeClass('active btn-danger');
      volume(elem, 'unmute');
    }
    volume(elem, 'up');
  });
  $(".remote_container .remoteVolumeMute").click(function() {
    var elem = document.getElementById("remoteVideo");

    if (!mute.state) {
      volume(elem, 'mute');
      $(".remote_container .remoteVolumeMute").addClass('active btn-danger');
    } else {
      volume(elem, 'unmute');
      $(".remote_container .remoteVolumeMute").removeClass('active btn-danger');
    }
  });
  function volume(elem, type) {
    var step = 0.1; // step for volume (minVol: 0; maxVol: 1)
    if (type == 'up' && elem.volume <= 1 - step) elem.volume += step;
    if (type == 'down' && elem.volume >= 0 + step) elem.volume -= step;
    if (type == 'mute') {
      mute.state = true;
      mute.before = elem.volume;
      elem.volume = 0;
    }
    if (type == 'unmute') {
      mute.state = false;
      if (mute.before >= step) elem.volume = mute.before;
      else elem.volume = 1;
    }
  }



  /**
   * add a contact
   */
  $('#modal_add_contact_btn').click(function() {
    var firstname = $('#input_login_firstname').val();
    var lastname = $('#input_login_lastname').val();
    var logindata = $('#input_logindata').val()

    // store contacts in localStorage
    contact.store(firstname, lastname, logindata);

    gui.hide.addContactModal();

    $.bootstrapGrowl('Stored new contact!', {
      type: 'success',
      offset: {
        from: 'bottom',
        amount: 60
      }
    });
  });



  /**
   * load contacts
   */
  $('#contact_btn').click(function() {
    $('#contact_list li').slice(2).remove();
    // contact handling
    $('#add_contact').click(function() {
      $('#modalAdContact').modal();
    });
    var contacts = contact.getAll();
    $.each(contacts, function(i, item) {
      $('#contact_list').append('<li style="width: 270px"><a><span class="text" value="' + contacts[i].logindata + '">' + contacts[i].name + ', ' + contacts[i].surname + '</span><span class="label label-success pull-right">online</span></a></li>');
    });

    $('#contact_list li a').click(function() {
      var selected_contact = $(this).children(".text").attr("value");
      $("#callTo").val(selected_contact);
    });
  });



  /**
   * helpfunctions
   */
  function inputCheck(element) {
    if ($(element).val().length !== 0) {
      $(element).parent().removeClass('has-error');
      return true;
    } else {
      $(element).parent().addClass('has-error');
      return false;
    }
  }



  /**
   * add remote name to box
   */
  function addRemote(evt) {
    $('#remoteName').html(evt.participant.identity.rtcIdentity);
    if (gui.states.audio) $('.remote_container .remoteAudioBtn').addClass('btn-success active');
    if (gui.states.video.remote) $('.remote_container .remoteVideoBtn').addClass('btn-success active');
    if (gui.states.chat) $('.remote_container .remoteChatBtn').addClass('btn-success active');
  }



  /*
  // example growl notification
  $.bootstrapGrowl("another message, yay!", {
    ele: 'body', // which element to append to
    type: 'info', // (null, 'info', 'danger', 'success')
    offset: {from: 'top', amount: 20}, // 'top', or 'bottom'
    align: 'right', // ('left', 'right', or 'center')
    width: 250, // (integer, or 'auto')
    delay: 4000, // Time while the message will be displayed. It's not equivalent to the *demo* timeOut!
    allow_dismiss: true, // If true then will display a cross to close the popup.
    stackup_spacing: 10 // spacing between consecutively stacked growls.
  });
  */
});

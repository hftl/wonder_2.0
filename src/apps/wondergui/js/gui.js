define(['config'], function(config) {

  /**
   * show/hide modules
   */
  var modules = {
    // module state machine
    states: {
      audio: false,
      video: {
        local: false,
        remote: false
      },
      chat: false,
      loggedIn: false,
    },

    // show modules
    show: {
      audio: function() {
        $('.remote_container .remoteAudioBtn').addClass("active btn-success");
        $('#localContainer .localMicBtn').addClass('active btn-success');
        $('#localContainer').fadeIn("slow").removeClass("hidden");
        $('#localVideo').fadeIn("slow").removeClass("hidden");
        $('#remoteVideo').removeClass("hidden");
        $('.remote_container').fadeIn("slow").removeClass("hidden");
        $('#navigation').collapse('hide');
        modules.states.audio = true;
      },
      localVideo: function() {
        $('#localContainer').fadeIn("slow").removeClass("hidden");
        $('#localVideo').fadeIn("slow").removeClass("hidden");
        $('#localContainer .localVideoBtn').addClass('active btn-success');
        $('#navigation').collapse('hide');
        modules.states.video.local = true;
      },
      remoteVideo: function() {
        $('#remoteVideo').removeClass("hidden");
        $('.remote_container').fadeIn("slow").removeClass("hidden");
        $('.remote_container .remotVideoBtn').addClass("active btn-success");
        $('#navigation').collapse('hide');
        modules.states.video.remote = true;
      },
      video: function() {
        modules.show.localVideo();
        modules.show.remoteVideo();
      },
      av: function() {
        modules.show.audio();
        modules.show.video();
      },
      chat: function() {
        modules.chat.initSummernote();
        $('.remote_container .remoteChatBtn').addClass("active btn-success");
        $('.remote_container').fadeIn("slow").removeClass("hidden");
        $('#chat').fadeIn("slow").removeClass("hidden");
        modules.states.chat = true;
      },
      all: function() {
        modules.show.av();
        modules.show.chat();
      },
      start: function() {
        $('#welcome_div ').fadeIn('slow ').removeClass('hidden ');
      },
      login: function() {
        modules.show.start();
        $('#login').fadeIn("slow").removeClass("hidden");
        modules.states.loggedIn = false;
      },
      // modals
      sendInvitationModal: function() {
        $("#callingModal").text("Calling...");
        $('#modalInviting').modal('show');
        document.getElementById('callSound').play();
      },
      receiveInvitationModal: function(caller) {
        $("#receivingModal").text(caller + " is trying to call you..");
        $('#modalInvite').modal('show');
        document.getElementById('ringingSound').play();
      },
      addContactModal: function() {
        $('#modalAdContact').modal('show');
      }
    },

    // hide modules
    hide: {
      audio: function() {
        $('#localContainer .localMicBtn').removeClass('active btn-success');
        modules.states.audio = false;
      },
      remoteVideo: function() {
        $('#remoteVideo').fadeOut('slow').addClass('hidden');
        $('.remote_container .remotVideoBtn').removeClass("active btn-success");
        modules.states.video.remote = false;
      },
      localVideo: function() {
        $('#localVideo').fadeOut("slow").addClass("hidden");
        $('#localContainer .localVideoBtn').removeClass('active btn-success');
        modules.states.video.local = false;
      },
      video: function() {
        modules.hide.remoteVideo();
        modules.hide.localVideo();
      },
      remote: function() {
        modules.hide.audio();
        modules.hide.remoteVideo();
        modules.hide.chat();
        $('.remote_container').addClass("hidden");
      },
      local: function() {
        modules.hide.localVideo();
        $('#localContainer').addClass("hidden");
      },
      av: function() {
        modules.hide.audio();
        modules.hide.video();
      },
      chat: function() {
        $('#chat').fadeOut("slow").addClass("hidden");
        $('.remote_container .remoteChatBtn').removeClass("active btn-success");
        modules.states.chat = false;
      },
      avc: function() {
        modules.hide.av();
        modules.hide.chat();
      },
      all: function() {
        modules.hide.remote();
        modules.hide.local();
        $('.after_login').fadeOut("slow").addClass("hidden");
        modules.show.login();
        modules.states.loggedIn = false;
      },
      start: function() {
        $('#welcome_div').fadeOut('slow').addClass('hidden');
      },
      login: function() {
        $('#domain-select').addClass("hidden");
        $('#login').addClass("hidden");
        $('.after_login').fadeIn("slow").removeClass("hidden");
        $('#modalIMS').modal('hide');
        modules.hide.start();
        $("callTo").focus();
        modules.states.loggedIn = true;
      },
      //modals
      sendInvitationModal: function() {
        document.getElementById('callSound').pause();
        $('#modalInviting').modal('hide');
      },
      receiveInvitationModal: function(caller) {
        document.getElementById('ringingSound').pause();
        $('#modalInvite').modal('hide');
      },
      addContactModal: function() {
        $('#modalAdContact').modal('hide');
      }
    },

    // chat operations
    chat : {
      getText: function() {
        var text = $('.summernote').code();
        return text;
      },
      getRecipient: function() {
        var rcpt = $('#remoteName').text();
        return rcpt;
      },
      entry: function(msg) {
        var label;
        var name;

        console.log(msg);
        var iDiv = document.getElementById('textChat');
        var innerDiv = document.createElement('li');
        innerDiv.className = 'list-group-item block-2';
        iDiv.appendChild(innerDiv);

        label = (msg.from == myIdentity.rtcIdentity ? 'default' : 'primary');
        name = (msg.from == myIdentity.rtcIdentity ? 'You' : msg.from);
        innerDiv.innerHTML = '<span class="label label-' + label + '">' + name + "</span>" + " <span>" + msg.body + "</span>";

        $("#datachannelmessage").attr("value", "");
        $("#datachannelmessage").text("");
        $("#textChat").animate({
          scrollTop: $("#textChat").height()
        }, 1000);
      },
      initSummernote: function() {
        $('.summernote').summernote({
          height: 100, // set editor height
          minHeight: 20, // set minimum height of editor
          //maxHeight: 300,             // set maximum height of editor
          focus: true, // set focus to editable area after initializing summernote
          toolbar: [
            //[groupname, [button list]]
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['insert', ['link']]
          ]
        });
      }
    }
  }

  return modules;
});

/**
 * webfinger-server
 *
 * Copyright 2015 by Danny Koppenhagen <mail@d-koppenhagen.de>
 */
var express = require('express');
var https = require('https');
var http = require('http');
var cors = require('cors');
var fs = require('fs');

var config = require('./config.js');
var wf = require('./service/webfingerService.js');

var app = express();
app.use(cors()); // enable all CORS requests

var port = config.port || 9110;

app.get('/', function(req, res) {
  res.send("webfinger-service is running");
});

app.get('/.well-known/host-meta', function(req, res) {
  wf.lookup(req, res);
});
app.get('/.well-known/host-meta.json', function(req, res) {
  wf.lookup(req, res);
});
app.get('/.well-known/webfinger', function(req, res) {
  wf.lookup(req, res);
});


if (config.protocol === 'https') {
  // listen for https requests
  var options = {
    key: fs.readFileSync('certs/webfinger.key'),
    cert: fs.readFileSync('certs/webfinger.crt'),
    csr: fs.readFileSync('certs/webfinger.csr')
  };
  https.createServer(options, app).listen(port, function(){
    console.log((new Date()) + " Webfinger (https) is listening on port ", port);
  });
} else {
  // listen for http requests
  http.createServer(app).listen(port, function(){
    console.log((new Date()) + " Webfinger (http) is listening on port ", port);
  });
}

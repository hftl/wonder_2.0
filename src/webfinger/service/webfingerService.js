var wf = require('webfinger');
var walk = require('walk');
var config = require('../config.js');
var msg;

exports.lookup = function(req, res) {
    console.log('new request:', req.query);
    res.setHeader('Access-Control-Allow-Origin', '*');

    var resource = req.query['resource'];

    // serve XML/XRD file
    if (resource === undefined) {
      res.setHeader('Content-Type', 'application/xml; charset=UTF-8');
      res.send('<XRD xmlns="http://docs.oasis-open.org/ns/xri/xrd-1.0">' +
               '  <Link rel="lrdd"' +
               '    type="application/xrd+xml"' +
               '    template="' +
               config.protocol + '://' +
               config.domain + '/webfinger/xrd/{uri}" />' +
               '</XRD>');
      return;
    }

    // set Header for json format
    res.setHeader('Content-Type', 'application/json; charset=UTF-8');

    var resourceTypePattern = /^[A-Za-z]+\:/;
    if (!resourceTypePattern.test(resource)) {
      // resource type (ie. acct:) not specified
      msg = {
        'error': 'malformed request',
        'details': 'request needs to be fomred like that: http://domain/.well-known/webfinger?resource=acct:user@domain'
      }

      res.send(msg);
      return;
    }

    var parts = resource.replace(/ /g,'').split(':'); // split 'acct'
    if (parts[0] === 'acct') { // currently the only resource type supported
      var address = parts[1].replace(/ /g,'').split('@');
      //console.log(address);
      if ((address[1] === undefined) || (address[1] !== config.domain)) {
        // domain portion of address must match this domain
        msg = {'error': 'invalid user address'}
        res.send(msg);
        return;
      } else {
        // get user response object
        var cwd    = process.cwd();
        var walker = walk.walk(cwd+'/data/acct', { followLinks: false });
        //console.log('directory:'+cwd+'/resource/acct');
        var sent = null;
        walker.on("file", function(root, stat, next) {
          var fileparts = stat.name.split(".json");
          // foreach file in directory, check for a matching username
          //console.log('filename:'+stat.name);
          if (fileparts[0] === address[0]) {
            // found userObj, send as response, and finish.
            var data = require('../data/acct/'+address[0]+'.json');
            console.log(data);
            res.send(data);
            sent=1;
            return;
          }
          next();
        });

        walker.on("end", function() {
          if (!sent){
           try {
            // try to find default json object
            var data = require('../data/default.json');
            console.log('found domain specific data: ', data);
            res.send(data);
            return;
           }
           catch (err) {
             err.error = 'no identity found';
             console.log(err);
             res.send(err);
           }
         }
          console.log("served identity.");
        });
      }
    } else {
      msg = {
        'error': 'wrong format',
        'missing': 'acct:',
        'details': 'request needs to be fomred like that: http://domain/.well-known/webfinger?resource=acct:user@domain'
      }
      // resource type not supported
      res.send(msg);
      return;
    }
};

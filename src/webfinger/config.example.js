module.exports = {
    port: '9110',
    ip: '', // ip address/interface to bind to, leave blank for all
    protocol: 'http', // http or https
    domain: 'localhost'
};

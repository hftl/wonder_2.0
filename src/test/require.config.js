require.config({
  baseUrl: '../../../dist',
  paths: {
    'wonder': 'wonder',
    'demand': '../../../test/unit/demandTest',
    'init': '../../../test/unit/initTest',
    'idp': '../../../test/unit/idpTest',
    'messageFactory': '../../../test/unit/messageFactoryTest',
    'message': '../../../test/unit/messageTest'
  }
});

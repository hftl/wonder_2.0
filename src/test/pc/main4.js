'use strict';

var localStream;
var pc1;
var pc2;
var msgbufpc1 = [];
var msgbufpc2 = [];

var startButton = document.getElementById('startButton');
var callButton = document.getElementById('callButton');
var hangupButton = document.getElementById('hangupButton');
callButton.disabled = true;
hangupButton.disabled = true;
startButton.disabled = false;
startButton.onclick = start;
callButton.onclick = call;
hangupButton.onclick = hangup;

var startTime;
var localVideo = document.getElementById('localVideo');
var remoteVideo = document.getElementById('remoteVideo');

localVideo.addEventListener('loadedmetadata', function() {
  trace('Local video videoWidth: ' + this.videoWidth +
    'px,  videoHeight: ' + this.videoHeight + 'px');
});

remoteVideo.addEventListener('loadedmetadata', function() {
  trace('Remote video videoWidth: ' + this.videoWidth +
    'px,  videoHeight: ' + this.videoHeight + 'px');
});

remoteVideo.onresize = function() {
  trace('Remote video size changed to ' +
    remoteVideo.videoWidth + 'x' + remoteVideo.videoHeight);
  // We'll use the first onsize callback as an indication that video has started
  // playing out.
  if (startTime) {
    var elapsedTime = window.performance.now() - startTime;
    trace('Setup time: ' + elapsedTime.toFixed(3) + 'ms');
    startTime = null;
  }
};


var offerOptions = {
  offerToReceiveAudio: 1,
  offerToReceiveVideo: 1
};

function getName(pc) {
  return (pc === pc1) ? 'pc1' : 'pc2';
}

function getOtherPc(pc) {
  return (pc === pc1) ? pc2 : pc1;
}

function gotStream(stream) {
  trace('Received local stream');
  // Call the polyfill wrapper to attach the media stream to this element.
  attachMediaStream(localVideo, stream);
  localStream = stream;
  callButton.disabled = false;
}

function start() {
  trace('Requesting local stream');
  startButton.disabled = true;
  console.log(navigator.mediaDevices);
  console.log(navigator);
  navigator.getUserMedia({
    audio: true,
    video: true
  },
  gotStream,
  function(e){
    alert('getUserMedia() error: ' + e.name);
  });
}

function call() {
  callButton.disabled = true;
  hangupButton.disabled = false;
  trace('Starting call');
  startTime = window.performance.now();
  var videoTracks = localStream.getVideoTracks();
  var audioTracks = localStream.getAudioTracks();
  if (videoTracks.length > 0) {
    trace('Using video device: ' + videoTracks[0].label);
  }
  if (audioTracks.length > 0) {
    trace('Using audio device: ' + audioTracks[0].label);
  }
  var servers = null;
  pc1 = new RTCPeerConnection(servers);
  trace('Created local peer connection object pc1');
  pc1.onicecandidate = function(e) {
    onIceCandidate(pc1, e);
  };

  pc2 = new RTCPeerConnection(servers);
  trace('Created remote peer connection object pc2');
  pc2.onicecandidate = function(e) {
    onIceCandidate(pc2, e);
  };

  pc1.oniceconnectionstatechange = function(e) {
    onIceStateChange(pc1, e);
  };
  pc2.oniceconnectionstatechange = function(e) {
    onIceStateChange(pc2, e);
  };
  pc2.onaddstream = gotRemoteStream;

  pc1.addStream(localStream);
  trace('Added local stream to pc1');

  trace('pc1 createOffer start');
  pc1.createOffer(onCreateOfferSuccess, onCreateSessionDescriptionError,
      offerOptions);
}

function onCreateSessionDescriptionError(error) {
  trace('Failed to create session description: ' + error.toString());
}

function onCreateOfferSuccess(desc) {
  trace('Offer from pc1\n' + desc.sdp);
  trace('pc1 setLocalDescription start');

  pc1.setLocalDescription(desc, function() {
    onSetLocalSuccess(pc1);
  }, onSetSessionDescriptionError);
  trace('pc2 setRemoteDescription start');
  setTimeout(function (){ // network timeout to send the local description from alice to bob
    console.log("-----------TIMEOUT");
    pc2.setRemoteDescription(new RTCSessionDescription(JSON.parse(JSON.stringify(desc))), function() {
      onSetRemoteSuccess(pc2);
    }, onSetSessionDescriptionError);
    trace('pc2 createAnswer start');
    // Since the 'remote' side has no media stream we need
    // to pass in the right constraints in order for it to
    // accept the incoming offer of audio and video.

    pc2.createAnswer(onCreateAnswerSuccess, onCreateSessionDescriptionError);
  }, 1000);
}

function onSetLocalSuccess(pc) {
  trace(getName(pc) + ' setLocalDescription complete');
}

function onSetRemoteSuccess(pc) {
  trace(getName(pc) + ' setRemoteDescription complete');
}

function onSetSessionDescriptionError(error) {
  trace('Failed to set session description: ' + error.toString());
}

function gotRemoteStream(e) {
  // Call the polyfill wrapper to attach the media stream to this element.
  attachMediaStream(remoteVideo, e.stream);
  trace('pc2 received remote stream');
}

function onCreateAnswerSuccess(desc) {
  trace('Answer from pc2:\n' + desc.sdp);
  trace('pc2 setLocalDescription start');
  pc2.setLocalDescription(desc, function() {
    onSetLocalSuccess(pc2);
  }, onSetSessionDescriptionError);
  trace('pc1 setRemoteDescription start');
  setTimeout(function (){ // Networtimeout to send the local description from bob to alice
    console.log("-----------TIMEOUT");
    pc1.setRemoteDescription(new RTCSessionDescription(JSON.parse(JSON.stringify(desc))), function() {
      onSetRemoteSuccess(pc1);
    }, onSetSessionDescriptionError);
    setTimeout(function (){ // Networtimeout to send the local description from bob to alice
      console.log("-----------TIMEOUT");
      iceallowdnowpc1();
      iceallowdnowpc2();
    }, 1000);
  }, 1000);
}

function onIceCandidate(pc, event) {
  if (event.candidate) {
    if (pc == pc1) {
        var cand = JSON.parse(JSON.stringify(event.candidate));
        msgbufpc1.push(cand);
    } else { // bobs ice candidates will also be held back
        var cand = JSON.parse(JSON.stringify(event.candidate));
        msgbufpc2.push(cand);
    }
  }
}

function iceallowdnowpc1(){
  for (var i = 0; i < msgbufpc1.length; i++) {
    pc2.addIceCandidate(
      new RTCIceCandidate(msgbufpc1[i]),
        function() {
          onAddIceCandidateSuccess(pc2);
        },
        function(err) {
          onAddIceCandidateError(pc2, err);
        }
    );
    trace(getName(pc2) + ' ICE candidate: \n' + msgbufpc1[i].candidate);
    msgbufpc1 = msgbufpc1.splice(i, 1);
  }
}
function iceallowdnowpc2(){
  for (var i = 0; i < msgbufpc2.length; i++) {
    pc1.addIceCandidate(
      new RTCIceCandidate(msgbufpc2[i]),
        function() {
          onAddIceCandidateSuccess(pc1);
        },
        function(err) {
          onAddIceCandidateError(pc1, err);
        }
    );
    trace(getName(pc1) + ' ICE candidate: \n' + msgbufpc2[i].candidate);
    msgbufpc2 = msgbufpc2.splice(i, 1);
  }
}

function onAddIceCandidateSuccess(pc) {
  trace(getName(pc) + ' addIceCandidate success');
}

function onAddIceCandidateError(pc, error) {
  trace(getName(pc) + ' failed to add ICE Candidate: ' + error.toString());
}

function onIceStateChange(pc, event) {
  if (pc) {
    trace(getName(pc) + ' ICE state: ' + pc.iceConnectionState);
    console.log('ICE state change event: ', event);
  }
}

function hangup() {
  trace('Ending call');
  pc1.close();
  pc2.close();
  pc1 = null;
  pc2 = null;
  hangupButton.disabled = true;
  callButton.disabled = false;
  startButton.disabled = false;
}

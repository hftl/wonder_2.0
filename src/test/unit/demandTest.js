/**
 * @fileOverview WebRTC Framework to facilitate the development of Applications which seamlessly interoperate with each other
 * @author Danny Koppenhagen <mail@d-koppenhagen.de>
 * @author Johannes Hamfler <jh@z7k.de>
 */

QUnit.test("Demand class", function( assert ) {

    function testcreate(param, expected) {
      assert.equal((new Demand(param) instanceof Object),
                    expected,
                    "Class type expected to be Object");
    }

    function testparam(param, expected) {
      assert.deepEqual( new Demand(param) ,
                    expected,
                    "Result expected to be demand object: "+expected);
    }

    // set all constraints to true if nothing is inside
    testcreate("", true);
    testcreate(null, true);
    testcreate({}, true);
    testcreate([], true);
    testcreate(undefined, true);

    // set all constraints to true if nothing is inside
    testparam("",{"in":{"audio":true, "video":true, "data":true},"out":{"audio":true, "video":true, "data":true}});
    testparam("all",{"in":{"audio":true, "video":true, "data":true},"out":{"audio":true, "video":true, "data":true}});
    testparam(null,{"in":{"audio":true, "video": true, "data":true},"out":{"audio":true, "video":true, "data":true}});
    testparam(undefined,{"in":{"audio":true, "video": true, "data":true},"out":{"audio":true, "video":true, "data":true}});
    testparam({},{"in":{"audio":true, "video": true, "data":true},"out":{"audio":true, "video":true, "data":true}});
    testparam([],{"in":{"audio":true, "video": true, "data":true},"out":{"audio":true, "video":true, "data":true}});


    var testparamTypes = ["audio", "video", "data"];
    var testparamValues = ["true", "false"];


    // set constraint related to the string
    testparam("audio", {"in":{"audio":true, "video":false, "data":false},"out":{"audio":true, "video":false, "data":false}});
    testparam("video", {"in":{"audio":false, "video":true, "data":false},"out":{"audio":false, "video":true, "data":false}});
    testparam("data", {"in":{"audio":false, "video":false, "data":true},"out":{"audio":false, "video":false, "data":true}});


    // test an array of demands as Stings
    testparam(["audio", "video", "data"], {"in":{"audio":true, "video":true, "data":true},"out":{"audio":true, "video":true, "data":true}});
    testparam(["audio", "video"], {"in":{"audio":true, "video":true, "data":false},"out":{"audio":true, "video":true, "data":false}});
    testparam(["video", "data"], {"in":{"audio":false, "video":true, "data":true},"out":{"audio":false, "video":true, "data":true}});
    testparam(["audio", "data"], {"in":{"audio":true, "video":false, "data":true},"out":{"audio":true, "video":false, "data":true}});


    // set constraint as an simple object
    testparam({"audio":true}, {"in":{"audio":true, "video":false, "data":false},"out":{"audio":true, "video":false, "data":false}});
    testparam({"video":true}, {"in":{"audio":false, "video":true, "data":false},"out":{"audio":false, "video":true, "data":false}});
    testparam({"data":true}, {"in":{"audio":false, "video":false, "data":true},"out":{"audio":false, "video":false, "data":true}});
    testparam({"audio":false}, {"in":{"audio":false, "video":false, "data":false},"out":{"audio":false, "video":false, "data":false}});
    testparam({"video":false}, {"in":{"audio":false, "video":false, "data":false},"out":{"audio":false, "video":false, "data":false}});
    testparam({"data":false}, {"in":{"audio":false, "video":false, "data":false},"out":{"audio":false, "video":false, "data":false}});

    testparam({"audio":true, "data":true}, {"in":{"audio":true, "video":false, "data":true},"out":{"audio":true, "video":false, "data":true}});
    testparam({"audio":true, "data":false}, {"in":{"audio":true, "video":false, "data":false},"out":{"audio":true, "video":false, "data":false}});
    testparam({"audio":false, "data":true}, {"in":{"audio":false, "video":false, "data":true},"out":{"audio":false, "video":false, "data":true}});
    testparam({"audio":false, "data":false}, {"in":{"audio":false, "video":false, "data":false},"out":{"audio":false, "video":false, "data":false}});

    testparam({"audio":true, "video":true}, {"in":{"audio":true, "video":true, "data":false},"out":{"audio":true, "video":true, "data":false}});
    testparam({"audio":true, "video":false}, {"in":{"audio":true, "video":false, "data":false},"out":{"audio":true, "video":false, "data":false}});
    testparam({"audio":false, "video":true}, {"in":{"audio":false, "video":true, "data":false},"out":{"audio":false, "video":true, "data":false}});
    testparam({"audio":false, "video":false}, {"in":{"audio":false, "video":false, "data":false},"out":{"audio":false, "video":false, "data":false}});

    testparam({"data":true, "video":true},  {"in":{"audio":false, "video":true, "data":true},"out":{"audio":false, "video":true, "data":true}});
    testparam({"data":true, "video":false},  {"in":{"audio":false, "video":false, "data":true},"out":{"audio":false, "video":false, "data":true}});
    testparam({"data":false, "video":true},  {"in":{"audio":false, "video":true, "data":false},"out":{"audio":false, "video":true, "data":false}});
    testparam({"data":false, "video":false},  {"in":{"audio":false, "video":false, "data":false},"out":{"audio":false, "video":false, "data":false}});

    testparam({"audio":true, "video":true, "data":true}, {"in":{"audio":true, "video":true, "data":true},"out":{"audio":true, "video":true, "data":true}});
    testparam({"audio":false, "video":false, "data":false}, {"in":{"audio":false, "video":false, "data":false},"out":{"audio":false, "video":false, "data":false}});

    testparam({"audio":true, "video":false, "data":false}, {"in":{"audio":true, "video":false, "data":false},"out":{"audio":true, "video":false, "data":false}});
    testparam({"audio":false, "video":true, "data":false}, {"in":{"audio":false, "video":true, "data":false},"out":{"audio":false, "video":true, "data":false}});
    testparam({"audio":false, "video":false, "data":true}, {"in":{"audio":false, "video":false, "data":true},"out":{"audio":false, "video":false, "data":true}});

    testparam({"audio":true, "video":true, "data":false}, {"in":{"audio":true, "video":true, "data":false},"out":{"audio":true, "video":true, "data":false}});
    testparam({"audio":true, "video":false, "data":true}, {"in":{"audio":true, "video":false, "data":true},"out":{"audio":true, "video":false, "data":true}});
    testparam({"audio":false, "video":true, "data":true}, {"in":{"audio":false, "video":true, "data":true},"out":{"audio":false, "video":true, "data":true}});

    testparam({ "audio": true, "data": false, "video": { "mandatory": { "maxWidth": 1920, "maxHeight": 1080, "minAspectRatio": 1.77, "minFrameRate": 3, "maxFrameRate": 64} }},
       {"in":{ "audio": true, "data": false, "video": { "mandatory": { "maxWidth": 1920, "maxHeight": 1080, "minAspectRatio": 1.77, "minFrameRate": 3, "maxFrameRate": 64}}},
        "out":{ "audio": true, "data": false, "video": { "mandatory": { "maxWidth": 1920, "maxHeight": 1080, "minAspectRatio": 1.77, "minFrameRate": 3, "maxFrameRate": 64} }}});
    testparam({ "audio": true, "data": false, "video": { "mandatory": { "maxFrameRate": 64, "maxWidth": 1920, "minFrameRate": 3} }},
       {"in":{ "audio": true, "data": false, "video": { "mandatory": { "maxWidth": 1920, "minFrameRate": 3, "maxFrameRate": 64}}},
        "out":{ "audio": true, "data": false, "video": { "mandatory": { "maxWidth": 1920, "minFrameRate": 3, "maxFrameRate": 64} }}});

    testparam({"in":{ "audio": true, "data": false, "video": { "mandatory": { "maxWidth": 1920, "minFrameRate": 3, "maxFrameRate": 64}}},
        "out":{ "audio": true, "data": false, "video": { "mandatory": { "maxWidth": 1920, "minFrameRate": 3, "maxFrameRate": 64} }}},
       {"in":{ "audio": true, "data": false, "video": { "mandatory": { "maxWidth": 1920, "minFrameRate": 3, "maxFrameRate": 64}}},
        "out":{ "audio": true, "data": false, "video": { "mandatory": { "maxWidth": 1920, "minFrameRate": 3, "maxFrameRate": 64} }}});


    // errors
    testparam("something unknown",{"in":{"audio":false, "video": false, "data":false},"out":{"audio":false, "video": false, "data":false}});
    testparam([null, "data"], {"in":{"audio":false, "video": false, "data":true},"out":{"audio":false, "video": false, "data":true}});
    testparam(["audio", ""], {"in":{"audio":true, "video": false, "data":false},"out":{"audio":true, "video": false, "data":false}});
    testparam(["something", {}], {"in":{"audio":false, "video": false, "data":false},"out":{"audio":false, "video": false, "data":false}});
    testparam([["", ""], ""], {"in":{"audio":false, "video": false, "data":false},"out":{"audio":false, "video": false, "data":false}});
    testparam([null], {"in":{"audio":false, "video": false, "data":false},"out":{"audio":false, "video": false, "data":false}});
    testparam({"audio":"something"}, {"in":{"audio":false, "video": false, "data":false},"out":{"audio":false, "video": false, "data":false}});

});

/**
 * @fileOverview WebRTC Framework to facilitate the development of Applications which seamlessly interoperate with each other
 * @author Danny Koppenhagen <mail@d-koppenhagen.de>
 * @author Johannes Hamfler <jh@z7k.de>
 */

QUnit.test("InitTest", function( assert ) {
    assert.expect(1);
    assert.equal( require,  window.require),"Class type expected to be WONDER");
});

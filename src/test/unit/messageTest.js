/**
 * @fileOverview WebRTC Framework to facilitate the development of Applications which seamlessly interoperate with each other
 * @author Danny Koppenhagen <mail@d-koppenhagen.de>
 * @author Johannes Hamfler <jh@z7k.de>
 */

QUnit.test("Message class", function( assert ) {

    function testcreate(param, expected) {
      assert.equal((new Message(param.from, param.to, param.body, param.type, param.context) instanceof Message),
                    expected,
                    "Class type expected to be Message");
    }

    function testtype(param, type, expected) {
      assert.equal( Object.prototype.toString.call(new Message(param.from, param.to, param.type, param.conversation)[type]),
                    expected,
                    "Parameter type "+type+" expected to be of type: " + expected);
    }

    function testparam(param, key, expected) {
      assert.deepEqual( new Message(param.from, param.to, param.type, param.conversation)[key],
                    expected,
                    "Parameter "+key+" expected to be: "+expected);
    }

    testcreate({from: "a@bc.de", to: "b", type: "typ", conversation: "1111"}, true);

    testtype  ({from: "a@bc.de", to: "b", type: "typ", conversation: "1111"}, "id",             "[object String]" );
    testtype  ({from: "a@bc.de", to: "b", type: "typ", conversation: "1111"}, "from",           "[object String]" );
    testtype  ({from: "a@bc.de", to: ["a@b.de"], type: "typ", conversation: "1111"}, "to",      "[object Array]" );
    testtype  ({from: "a@bc.de", to: "b", type: "typ", conversation: "1111"}, "type",           "[object String]" );
    testtype  ({from: "a@bc.de", to: "b", type: "typ", conversation: "1111"}, "conversationId", "[object String]" );

    testparam ({from: "a@bc.de", to: "b", type: "typ", conversation: "1111"}, "from",           "a@bc.de");
    testparam ({from: "a@bc.de", to: ["a@a.de","c@c.de"], type: "typ", conversation: "1111"}, "to", ["a@a.de","c@c.de"]);
    testparam ({from: "a@bc.de", to: "b", type: "typ", conversation: "1111"}, "type",           "typ");
    testparam ({from: "a@bc.de", to: "b", type: "typ", conversation: "1111"}, "conversationId", "1111");
});

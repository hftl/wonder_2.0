/**
 * @fileOverview WebRTC Framework to facilitate the development of Applications which seamlessly interoperate with each other
 * @author Danny Koppenhagen <mail@d-koppenhagen.de>
 * @author Johannes Hamfler <jh@z7k.de>
 */

QUnit.test("MessageFactory class", function( assert ) {

    //assert.expect(1);
    function testcreate(param, expected) {
      assert.equal((new MessageFactory(param.from, param.to, param.conversationId, param.constraints) instanceof MessageFactory),
                    expected,
                    "Class constructor expected to to return an instance of itself");
    }

    // test also typeof because a string hasn't neccessarily a prototype or is an object
    function testtype(param, type, expected) {
      mf = new MessageFactory();
      assert.equal( typeof (mf[param.func](param.from, param.to, param.conversationId, param.constraints)[type]) === expected ||
                    (mf[param.func](param.from, param.to, param.conversationId, param.constraints)[type]) instanceof expected,
                    true,
                    "Parameter type "+type+" in function "+param.func+" expected to be of type: " + expected);
    }

    function testreturn(param, expected) {
      mf = new MessageFactory();
      assert.equal( mf[param.func](param.from, param.to, param.conversationId, param.constraints) instanceof expected,
                    true,
                    "Return type in function "+param.func+" expected to be of type: " + expected);
    }

    function testparam(param, key, expected) {
      mf = new MessageFactory();
      assert.deepEqual( mf[param.func](param.from, param.to, param.conversationId, param.constraints)[key],
                    expected,
                    "Parameter "+key+" in function "+param.func+" expected to be: "+expected);
    }

    // constructor is working no matter if parameters are present as it is only a function holding class
    testcreate({from: "a@a.de", to: "b@b.de", conversationId: "1111", constraints:{in:{audio:true,video:true},out:{file:true,chat:false}}}, true);
    testcreate({}, true);

    function testMsgFactoryFunction(functionname, testMisc){
      // testing the type of variables in the message
      testtype  ({func: functionname, from: new Identity(), to: new Identity(), conversationId: guid(),
                  constraints:{in:{audio:true,video:true},out:{file:true,chat:false}}},
                  "id",
                  "string" ); // Message must have an id of type string
      testtype  ({func: functionname, from: new Identity(), to: new Identity(), conversationId: guid(),
                  constraints:{in:{audio:true,video:true},out:{file:true,chat:false}}},
                  "from",
                  Identity ); // to should be set to the identity
      testtype  ({func: functionname, from: new Identity(), to: new Identity(), conversationId: guid(),
                  constraints:{in:{audio:true,video:true},out:{file:true,chat:false}}},
                  "to",
                  Identity ); // from should be set to the identity
      testtype  ({func: functionname, from: new Identity(), to: new Identity(), conversationId: guid(),
                  constraints:{in:{audio:true,video:true},out:{file:true,chat:false}}},
                  "conversationId",
                  "string" ); // conversationId is created by guid which should return a string
      if (testMisc) testtype  ({func: functionname, from: new Identity(), to: new Identity(), conversationId: guid(),
                  constraints:{in:{audio:true,video:true},out:{file:true,chat:false}}},
                  "misc",
                  "object" ); // deep test would be too much here


      // test success return value
      testreturn({func: functionname, from: new Identity(), to: new Identity(), conversationId: guid(),
                  constraints:{in:{audio:true,video:true},out:{file:true,chat:false}}},
                  Message ); // from needs to be an Identity

      // test error retun values
      testreturn({func: functionname, from: "a@b.de", to: new Identity(), conversationId: guid(),
                  constraints:{in:{audio:true,video:true},out:{file:true,chat:false}}},
                  Error ); // from needs to be an Identity
      testreturn({func: functionname, from: new Identity(), to: "a@b.de", conversationId: guid(),
                  constraints:{in:{audio:true,video:true},out:{file:true,chat:false}}},
                  Error ); // to needs to be an Identity or array of identities
      testreturn({func: functionname, from: new Identity(), to: ["a","b"], conversationId: guid(),
                  constraints:{in:{audio:true,video:true},out:{file:true,chat:false}}},
                  Error ); // to needs to be an Identity or array of identities
      testreturn({func: functionname, from: new Identity(), to: new Identity(), conversationId: 1,
                  constraints:{in:{audio:true,video:true},out:{file:true,chat:false}}},
                  Error ); // conversationId needs to be a String
      if (testMisc) testreturn({func: functionname, from: new Identity(), to: new Identity(), conversationId: guid(),
                  constraints:"c"},
                  Error ); // constraints need to be an object

      // test paremeters to be correctly set
      testparam ({func: functionname, from: new Identity("a@b.de"), to: new Identity(), conversationId: guid(),
                  constraints:{in:{audio:true,video:true},out:{file:true,chat:false}}},
                  "from",
                   new Identity("a@b.de")); //Identity set correctly
      testparam ({func: functionname, from: new Identity(), to: new Identity("c"), conversationId: guid(),
                 constraints:{in:{audio:true,video:true},out:{file:true,chat:false}}},
                 "to",
                  new Identity("c")); //Identity to is also correctly set
      testparam ({func: functionname, from: new Identity(), to: [new Identity("c")], conversationId: guid(),
                 constraints:{in:{audio:true,video:true},out:{file:true,chat:false}}},
                 "to",
                  [new Identity("c")]); // to can olso be an array of identities
      var id = guid(); // save guid so we can compare it
      testparam ({func: functionname, from: new Identity(), to: new Identity(), conversationId: id,
                 constraints:{in:{audio:true,video:true},out:{file:true,chat:false}}},
                 "conversationId",
                  id); // to can olso be an array of identities
      if (testMisc) testparam ({func: functionname, from: new Identity(), to: new Identity(), conversationId: guid(),
                  constraints:{in:{audio:true,video:true},out:{file:true,chat:false}}},
                  "misc",
                  {constraints:{in:{audio:true,video:true},out:{file:true,chat:false}}}); // expect the constraints to be wrapped in an object
    }

    testMsgFactoryFunction("invitation", true);
    testMsgFactoryFunction("accepted", true);
    testMsgFactoryFunction("declined", false); // don't test conversationId
    testMsgFactoryFunction("bye", false);
    testMsgFactoryFunction("updateConstraints", true);
    testMsgFactoryFunction("presence", false);



/*
    testparam ({from: "a@bc.de", to: "b", type: "typ", conversation: "1111"}, "from",           "a@bc.de");
    testparam ({from: "a@bc.de", to: ["a@a.de","c@c.de"], type: "typ", conversation: "1111"}, "to", ["a@a.de","c@c.de"]);
    testparam ({from: "a@bc.de", to: "b", type: "typ", conversation: "1111"}, "type",           "typ");
    testparam ({from: "a@bc.de", to: "b", type: "typ", conversation: "1111"}, "conversationId", "1111");
    */
});

/**
 * @fileOverview WebRTC Framework to facilitate the development of Applications which seamlessly interoperate with each other
 * @author Danny Koppenhagen <mail@d-koppenhagen.de>
 * @author Johannes Hamfler <jh@z7k.de>
 */

QUnit.test("Idp class", function( assert ) {
    assert.expect(32);
    function testcreate(param, expected) {
      assert.equal((new Idp(param.remoteIdp) instanceof Idp),
                    expected,
                    "Class type expected to be Idp");
    }

    function testtype(param, key, expected) {
      assert.equal( Object.prototype.toString.call(new Idp(param.remoteIdp)[key]),
                    expected,
                    "Parameter key "+key+" expected to be of type: " + expected);
    }

    function testparam(param, key, expected) {
      assert.deepEqual( new Idp(param.remoteIdp)[key],
                    expected,
                    "Parameter "+key+" expected to be: "+expected);
    }

    function testParamNot(param, key, expected) {
      assert.notEqual( new Idp(param.remoteIdp)[key],
                    expected,
                    "Parameter "+key+" expected to be: "+expected);
    }

    // as Promises are Handlers for asynchronous functions we need
    // to tell QUnit that we have one here so that the assert counter
    // knows when to stop handling the asynchronous function as
    // asynchronous and return to synchronous code as long as
    // the functions waits for an event to continue working
    function testGetIdentityPromise(param, expected) {
      var done = assert.async();
      idp = new Idp(param.remoteIdp);
      idp.resolvedIdentities = param.resolvedIdentities;
      var promise = idp.getIdentity(param.rtcIdentity);
      return promise
        .then(function(identity) {
          assert.equal( Object.prototype.toString.call(identity),
                        expected,
                        "Return type expected to be: " + expected);
          done();
        })
        .catch(function(e) {
          assert.equal( Object.prototype.toString.call(e),
                        expected,
                        "Return expected to be: " + expected);
          done();
        });
    }

    function testaskRemoteIdpPromise(param, expected) {
        var done = assert.async();
        idp=new Idp(param.remoteIdp);
        var promise = idp.askRemoteIdp(param.rtcIdentity);
        return promise
            .then(function(identity) {
              assert.equal( Object.prototype.toString.call(identity),
                            expected,
                            "Return type expected to be: " + expected);
              done();
            })
            .catch(function(e) {
              assert.equal( Object.prototype.toString.call(e),
                            expected,
                            "Return expected to be: " + expected);
              done();
            });
    }

    function testgetMsgStubPromise(param, expected) {
        var done = assert.async();
        idp=new Idp(param.remoteIdp);
        var promise = idp.getMsgStub(param.msgStubUrl);
        return promise
            .then(function(msgStub) {
              assert.equal( Object.prototype.toString.call(msgStub),
                            expected,
                            "Return type expected to be: " + expected);
              done();
            })
            .catch(function(e) {
              assert.equal( Object.prototype.toString.call(e),
                            expected,
                            "Return expected to be: " + expected);
              done();
            });
    }




    // Idp constructor tests
    testcreate({remoteIdp: "http://idp.id/idp.php"}, true);

    testtype  ({remoteIdp: "http://idp.id/idp.php"}, "remoteIdp",          "[object String]" );
    testtype  ({remoteIdp: "http://idp.id/idp.php"}, "resolvedIdentities", "[object Array]" );

    testparam ({remoteIdp: "http://idp.id/idp.php"}, "remoteIdp",          "http://idp.id/idp.php");
    testparam ({remoteIdp: "//"},                    "remoteIdp",          "//");
    testparam ({remoteIdp: "http://idp.id/idp.php"}, "resolvedIdentities", []);
    testparam ({remoteIdp: "1.1.1.1"},               "remoteIdp",          "1.1.1.1");

    testParamNot({remoteIdp: ""},                    "remoteIdp",          "");
    testParamNot({remoteIdp: undefined},             "remoteIdp",          undefined);
    testParamNot({remoteIdp: {}},                    "remoteIdp",          {});
    testParamNot({remoteIdp: []},                    "remoteIdp",          []);
    testParamNot({remoteIdp: [{},{}]},               "remoteIdp",          [{},{}]);
    testParamNot({remoteIdp: [1,{a:2},"s"]},         "remoteIdp",          [1,{a:2},"s"]);
    testParamNot({remoteIdp: {remoteIdp:"http://i.i/i.php"}}, "remoteIdp",{remoteIdp:"http://i.i/i.php"});

    // getIdentity
    testGetIdentityPromise({  remoteIdp:"http://"+window.location.hostname+":5000/users?jsonp=define&filter_rtcIdentity=",
                              resolvedIdentities:[],
                              rtcIdentity: "a@nodejs.wonder"},
                              Object.prototype.toString.call(new Identity())); // definately receive an Identity inside the Promise
    testGetIdentityPromise({  remoteIdp:"http://000.000t:5000/users?jsonp=define&filter_rtcIdentity=",
                              resolvedIdentities:[],
                              rtcIdentity: "a@nodejs.wonder"},
                              "[object Error]"); // wrong ip address
    testGetIdentityPromise({  remoteIdp:"http://"+window.location.hostname+":5000/users?jsonp=define&filter_rtcIdentity=",
                              resolvedIdentities:[],
                              rtcIdentity: "231.p-a@nodejs.wonder"},
                              Object.prototype.toString.call(new Identity())); // the name doesnt matter to get an identity
    testGetIdentityPromise({  remoteIdp:"http://"+window.location.hostname+":5000/users?jsonp=define&filter_rtcIdentity=",
                              resolvedIdentities:[],
                              rtcIdentity: "a@b"},
                              "[object Error]"); // the domain is not available and the error is thrown further up
    testGetIdentityPromise({  remoteIdp:"http://no@ip_addressüüü:5000/users?jsonp=define&filter_rtcIdentity=",
                              resolvedIdentities:[{rtcIdentity:"z@z"}],
                              rtcIdentity: "z@z"},
                              "[object Object]"); // if the identity is pre-initialized return it and dont care about anything else

    // askRemoteIdp
    testaskRemoteIdpPromise({ remoteIdp:"http://nooo@ip@adressö:5000/users?jsonp=define&filter_rtcIdentity=",
                              resolvedIdentities:[{rtcIdentity:"z@z"}],
                              rtcIdentity: "älicéöüß@nodejs.wonder"},
                              "[object Error]"); //wrong server address
    testaskRemoteIdpPromise({ remoteIdp:"http://"+window.location.hostname+":5000/users?jsonp=define&filter_rtcIdentity=",
                              resolvedIdentities:[{rtcIdentity:"z@z"}],
                              rtcIdentity: "älicéöüß@nodejs.wonder"},
                              "[object Object]"); //only the domain of the rtcIdentity matters
    testaskRemoteIdpPromise({ remoteIdp:"http://"+window.location.hostname+":5000/users?jsonp=define&filter_rtcIdentity=",
                              resolvedIdentities:[],
                              rtcIdentity: "älicéöüß@nodejs.wonder"},
                              "[object Object]"); // uninitialized resolvedIdentities doesnt matter
    testaskRemoteIdpPromise({ remoteIdp:"http://"+window.location.hostname+":5000/users?jsonp=define&filter_rtcIdentity=",
                              resolvedIdentities:[{rtcIdentity:"z@z"}],
                              rtcIdentity: "t@NODEJS.WONDER"},
                              "[object Object]"); // case sensitivity doesnt matter
    testaskRemoteIdpPromise({ remoteIdp:"http://"+window.location.hostname+":5/users?jsonp=define&filter_rtcIdentity=",
                              resolvedIdentities:[{rtcIdentity:"z@z"}],
                              rtcIdentity: "z@z"},
                              "[object Error]"); // port and rtcIdentity domain is wrong; network error in logs
    testaskRemoteIdpPromise({ remoteIdp:"http://"+window.location.hostname+":5000/users?jsonp=define&filter_rtcIdentity=",
                              resolvedIdentities:[{rtcIdentity:"z@z"}],
                              rtcIdentity: "z@z"},
                              "[object Error]"); //  only rtcIdentity domain is wrong
    testaskRemoteIdpPromise({ remoteIdp:"http://"+window.location.hostname+":5000/users?jsonp=func&filter_rtcIdentity=",
                              resolvedIdentities:[{rtcIdentity:"z@z"}],
                              rtcIdentity: "z@z"},
                              "[object Error]"); // jsonp param wrong
    testaskRemoteIdpPromise({ remoteIdp:"http://"+window.location.hostname+":5000/users?jsonp=define&rtcIdentity=",
                              resolvedIdentities:[{rtcIdentity:"z@z"}],
                              rtcIdentity: "z@z"},
                              "[object Error]"); // rtcIdentity param-key wrong

    // getMsgStub
    testgetMsgStubPromise({   remoteIdp:"http://"+window.location.hostname+":5000/users?jsonp=define&rtcIdentity=",
                              resolvedIdentities:[{rtcIdentity:"z@z"}],
                              rtcIdentity: "z@z",
                              msgStubUrl: "http://"+window.location.hostname+":8080/stubs/MessagingStub_NodeJS.js"},
                              "[object Object]"); // dependent of Idp; if "Object Undefined" then wrong Wrapper with define(); inside MsgStub-File
    testgetMsgStubPromise({   remoteIdp:"",
                              rtcIdentity: "",
                              msgStubUrl: "http://"+window.location.hostname+":8080/stubs/MessagingStub_NodeJS.js"},
                              "[object Object]"); // independent of Idp; if "Object Undefined" then wrong Wrapper with define(); inside MsgStub-File
    testgetMsgStubPromise({   remoteIdp:"http://"+window.location.hostname+":0/rtcIdentity=",
                              resolvedIdentities:[{rtcIdentity:"z@z"}],
                              rtcIdentity: "z@zzzzzz",
                              msgStubUrl: "http://"+window.location.hostname+":0/stubs/MessagingStub_NodeJS.js"},
                              "[object Error]"); // Wrong port
    testgetMsgStubPromise({   msgStubUrl: "http://"+window.location.hostname+":8080/nostubbbbbb.js"},
                              "[object Error]");
    testgetMsgStubPromise({   msgStubUrl: "http://a:8080/stubs/MessagingStub_NodeJS.js"},
                              "[object Error]"); // wrong address of messaging server






});

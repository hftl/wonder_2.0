import java.io.File;

import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.support.ui.Sleeper;

public class Main {

	public static void main(String[] args) {
		
		// no driver needed for Firefox
		System.setProperty("webdriver.chrome.driver", "chromedriver");
		
		/*
		//File fffb = new File("/home/jh/Downloads/firefox-beta/firefox/firefox"); //no classes: 41.0b4
		//File fffb = new File("/home/jh/Downloads/firefox42/firefox/firefox"); //no classes: 42.0a2 (2015-08-27)
		File fffb = new File("/home/jh/Downloads/firefox43/firefox/firefox");//43.0a1 (2015-08-27)
		FirefoxBinary ffb = new FirefoxBinary(fffb);
		
		File fffp = new File("/home/jh/.mozilla/firefox/lbp4ay74.default");
		FirefoxProfile ffp = new FirefoxProfile();//fffp);
		
		FirefoxDriver ff = new FirefoxDriver(ffb, ffp);
		*/
		
		
		ChromeOptions options = new ChromeOptions();
		options.addArguments("use-fake-ui-for-media-stream");
		
		ChromeDriver c = new ChromeDriver(options);
		
		
		
		c.navigate().to("http://schubse.net:8080/apps/minimal/");
		try {
			Thread.sleep(1000); // neccessary as race conditions havent been handled yet in the GUI
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		c.findElementById("loginBtn").click();
		c.findElementById("videoCallBtn").click();
		
		//ff.findElementById("loginBtn").click();
		
		
		/*
		// google works
		ff.navigate().to("http://google.de");
		ff.findElement(By.name("q")).sendKeys("lalalala");
		ff.findElementByName("btnG").click();
		*/
		
		
		/*// wondergui doesnt work since latest commits
		ff.navigate().to("http://schubse.net:8080/apps/wondergui/");
		 
		ff.findElementByXPath("/html/body/nav[1]/div/div[2]/ul[1]/li/a").click();//select domain
		ff.findElementByXPath("/html/body/nav[1]/div/div[2]/ul[1]/li/ul/li[1]/a").click();//nodejs
		ff.findElementByXPath("/html/body/nav[1]/div/div[2]/ul[1]/form/div/div/div/span/button").click();//login
		
		
		ff.findElementByXPath("/html/body/nav[1]/div/div[2]/ul[1]/li/ul/li[1]/a").click();// nodejs
		ff.findElementByXPath("/html/body/nav[1]/div/div[2]/ul[1]/form/div/div/div/input").clear();
		ff.findElementByXPath("/html/body/nav[1]/div/div[2]/ul[1]/form/div/div/div/input").sendKeys("a@nodejs.wonder");
		ff.findElement(By.xpath("/html/body/nav[1]/div/div[2]/ul[1]/form/div/div/div/span/button")).click(); // login
		//ff.findElementById("loginText").sendKeys("a@wonder.nodejs");
		//ff.findElementById("loginButton").click();
		*/
		
		
		
		
		//ff.close();
		//c.close();
	}

}

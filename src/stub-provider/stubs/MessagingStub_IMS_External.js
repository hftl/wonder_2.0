/**
 * Defines the stub as a Module to be loaded via requireJS.
 * This avoids name-spacing issues.
 * @author Steffen Druesedow <Steffen.Druesedow@telekom.de>
 * @returns {MessagingStub_IMS_External}
 */

'use strict';

define(function(require, exports, module) {

  class MessagingStub_IMS_External {
    /*********************************************************************************
     * Messaging Stub Class
     * For websocket
     */
    constructor() {
      this.ownRtcIdentity;
      this.credentials;

      this.websocket;
    }

    /**
     * Sends the specified message.
     * @param Message : message ... Message to send.
     */
    sendMessage(message) {

      // From and To Identities are changed into strings containing rtcIdentities
      // To is always an array even if it wasnt.
      message.from = message.from.rtcIdentity;

      if (message.to instanceof Array)
        message.to.every(function(element, index, array) {
          array[index] = element.rtcIdentity;
        });
      else
        message.to = new Array(message.to.rtcIdentity);

      // hack for compatibility with old MessagingServer, which expects a contextID in the message
      if (!message.contextId)
        message.contextId = message.conversationId;

      this.websocket.send(JSON.stringify(message));
    };

    /**
     * Creates the connection, connects to the server and establish the callback to the listeners on new message.
     * @param Identity : manager ...
     * @param ???????? : credentials ... credentials to connect to the server.
     */
    connect(ownRtcIdentity, credentials, msgSrv, callbackFunction) {
      this.ownRtcIdentity = ownRtcIdentity;
      this.credentials = credentials;


      // If connect was already executed succesfully, it won't try to connect again, just execute the callback.
      if (this.websocket) {
        console.log("Websocket connection already opened, executing callback function: ");
        callbackFunction();
        return;
      }

      console.log('Opening channel: ' + msgSrv);
      this.websocket = new WebSocket(msgSrv);

      this.websocket.onopen = function() {
        console.log("Websocket connection opened");
        callbackFunction();
      };
      this.websocket.onerror = function() {
        console.log("Websocket connection error");
      };
      this.websocket.onclose = function() {
        console.log("Websocket connection closed");
      };

      // Maps the websocket listener to the MessagingStub listeners, and notifies filtering
      // by contextId
      var that = this;
      this.websocket.onmessage = function(full_message) {
        // IF it doesnt have contextID, it is the application.
        var message = JSON.parse(full_message.data);
        that.onMessage(message);
      };
    }

    /**
     * Disconnects from the server.
     */
    disconnect() {
      this.websocket.close();
      this.websocket = null;
    };
  }
  return new MessagingStub_IMS_External();
});

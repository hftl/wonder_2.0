/**
 * Defines the stub as a Module to be loaded via requireJS.
 * This avoids name-spacing issues.
 * @author Steffen Druesedow <Steffen.Druesedow@telekom.de>
 * @returns {MessagingStub_IMS_Local}
 */

'use strict';

define(function(require, exports, module) {

  class MessagingStub_IMS_Local {

    /*********************************************************************************
     * Messaging Stub Class
     * For IMS2Cloud
     *
     * This file is a translating stub between the Messages from the Wonder-Lib and
     * the IMS2Cloud.
     * For this purpose it implements the MessagingStub-Interface of the Wonder-Lib.
     */
    constructor() {

      this.CON = {
        WONDER: "wonder"
      };

      this.IMSCM_COMMANDS = {
        START_CALL: "startwebrtccallrequest",
        ANSWER_CALL: "receivewebrtccallresponse",
        BYE: "stopsessionrequest",
        UPDATE: "callupdaterequest",
        UPDATE_RESPONSE: "callupdateresponse",
        REGISTER_RESPONSE: "registerresponse"
      };

      this.IMSCM_STATUS = {
        NONE: 0,
        OK: 10,
        DENIED: 30
      };

      this.ownRtcIdentity;
      this.credentials;
      this.connectCallbackFunction;
      this.errorConnectCallbackFunction;

      // keeps the mapping of context-IDs to sessionIDs
      this.sessionIDs = {};
      // for communication with non-wonder clients, the contextID's are missing
      // in incoming invites or in 200oKs, for such cases we keep a mapping of sessionIDs to contextIDs
      this.contextIDs = {};
      this.inviteContextId;
      this.socket;

      this.updateSent = 0;
      this.updateReceived = 0;
    }


    /**
     * SendMessage
     * wraps the given Wonder-message into an IMS2Cloud message and sends it through the websocket
     * @param message
     */
    sendMessage(message) {
      var that = this;

      function extractRtcIdentities(identities) {
        // ensure "to" is an array
        var to = [];
        to.push(identities);
        to.every(function(element, index, array) {
          array[index] = element.rtcIdentity;
        });
        return to;
      };

      function createAttributes(message, callStatus, sessionId) {
        var attributes = new Object();
        attributes.callStatus = callStatus;
        if (message.misc && message.misc.sessionDescription)
          attributes.sdp = message.misc.sessionDescription.sdp;
        if (sessionId)
          attributes.sessionId = sessionId;
        // wonder-specific parts
        attributes.contentType = that.CON.WONDER;
        message.to = extractRtcIdentities(message.to);
        attributes.body = message;
        attributes.body.from = message.from.rtcIdentity;

        return attributes;
      };

      var msg;
      // check for known WONDER-messages
      switch (message.type) {
        case MessageType.invitation:
          var attributes = createAttributes(message, "offer");
          attributes.sipUri = message.to[0];
          msg = this.ims2CloudMessage(this.IMSCM_COMMANDS.START_CALL, this.IMSCM_STATUS.NONE, attributes);
          break;

        case MessageType.accepted:
          var attributes = createAttributes(message, "answer", this.sessionIDs[message.conversationId]);
          msg = this.ims2CloudMessage(this.IMSCM_COMMANDS.ANSWER_CALL, this.IMSCM_STATUS.OK, attributes);
          break;

        case MessageType.declined:
          var attributes = createAttributes(message, "answer", this.sessionIDs[message.conversationId]);
          msg = this.ims2CloudMessage(this.IMSCM_COMMANDS.ANSWER_CALL, this.IMSCM_STATUS.DENIED, attributes);
          break;

        case MessageType.bye:
          var attributes = createAttributes(message, "", this.sessionIDs[message.conversationId]);
          attributes.info = "hangup";
          delete attributes.callStatus;
          msg = this.ims2CloudMessage(this.IMSCM_COMMANDS.BYE, this.IMSCM_STATUS.NONE, attributes);
          break;

        case MessageType.connectivityCandidate:
          // send this as update message --> this will map to IMS Info message
          var attributes = createAttributes(message, "update", this.sessionIDs[message.conversationId]);
          attributes.sipUri = message.to[0];
          msg = this.ims2CloudMessage(this.IMSCM_COMMANDS.UPDATE, this.IMSCM_STATUS.NONE, attributes);
          this.updateSent++;
          console.log("################# connectivityCandidates SENT: " + this.updateSent);
          break;
        default:
          // this is a non-wonder msg - send it unchanged
          //				console.log("this is a non-wonder message --> forward it unchanged");
          msg = message;
          break;
      }

      if (msg && this.socket) {
        this.doSend(msg);
      }
    }


    connect(ownRtcIdentity, credentials, msgSrv, callbackFunction, errorCallbackFunction) {
      this.initialWSUri = msgSrv;
      this.ownRtcIdentity = ownRtcIdentity;
      this.credentials = credentials;
      this.connectCallbackFunction = callbackFunction;
      this.errorConnectCallbackFunction = errorCallbackFunction;

      // If connect was already executed succesfully, it won't try to connect again, just execute the callback.
      if (this.socket) {
        console.log("Websocket connection already opened, executing callback function: ");
        callbackFunction();
        return;
      }

      console.log("Opening Init-API channel at: " + this.initialWSUri);
      var self = this;
      this.socket = new WebSocket(this.initialWSUri);
      this.socket.onopen = function() {
        var attributes = new Object();
        attributes.user = self.credentials.user;
        attributes.pubID = "";
        attributes.role = self.credentials.role;
        attributes.pass = self.credentials.pass;
        attributes.realm = self.credentials.realm;
        attributes.pcscf = self.credentials.pcscf;
        var register_request = self.ims2CloudMessage("registerrequest", 0, attributes);
        self.doSend(register_request);
      };

      this.socket.onerror = function() {
        console.log("Init-Websocket connection error");
        if (errorCallbackFunction)
          errorCallbackFunction();
      };
      this.socket.onclose = function() {
        console.log("Init-Websocket connection closed");
      };

      this.socket.addEventListener("message", function(event) {
        self.onSocketMessage(event, self);
      }, false);
    };

    /**
     * Incoming message from IMSC.
     * We have to check for the content-type.
     * If it is "wonder" then we take the attribute.body as WonderMessage.
     * If not, then we create a corresponding Message from all available
     */
    onSocketMessage(event, self) {
      var msg = JSON.parse(event.data);
      var wonderMsg;
      if (msg.attributes.contentType === this.CON.WONDER) {
        // the wonder msg is just a string in the body attribute --> need to parse it as JSON
        wonderMsg = JSON.parse(msg.attributes.body);
        //                    console.log(JSON.stringify(wonderMsg));
      }
      this.handleMessage(msg, wonderMsg, self);
    };

    handleMessage(msg, wonderMsg, self) {
      console.log("<<< received: " + msg.cmd + "  with content-type: " + msg.attributes.contentType);

      switch (msg.cmd) {
        case 'registerresponse':
          // This must be handled internally in this lib, and a final event must be emitted to app-layer
          if (msg.status == 10) {
            // fix for handling internal vs. public ip -> use configured ip/port from initial WS
            // only take id from returned URL
            var arr = msg.attributes.wsuri.split("/");
            var wsID = arr[arr.length - 1];
            var controlWSUri = this.initialWSUri + "/" + wsID;
            console.log("got wsuri = " + controlWSUri + " --> opening Control-Api channel");
            var s = new WebSocket(controlWSUri);
            self.socket = s;
            s.addEventListener("message", function(event) {
              self.onSocketMessage(event, self);
            }, false);
            s.onopen = function() {
              self.connectCallbackFunction(msg);
            };
            s.onclose = function() {
              console.log("WS socket closed")
            };
          } else {
            console.log("closing WS");
            self.socket.close();
            self.socket = null;
            self.errorConnectCallbackFunction(msg);
          }
          break;
        case 'hellorequest':
          console.log("not implemented");
          break;
        case 'startwebrtccallresponse':
          // take the sessionID from the response and add the mapping with the remembered contextID
          console.log("S->C: startwebrtccallresponse' with CallID : " + msg.attributes.sessionId);
          console.log("assigned wonder-contextId = " + self.inviteContextId);
          self.contextIDs[msg.attributes.sessionId] = self.inviteContextId;
          break;
        case 'stopsessionresponse':
          break;
          // the following messages can contain
        case 'receivewebrtccallrequest': // this can be incoming offer or incoming answer
          /*
           * This might be the offer/answer from a pure IMS client, that does not hold Wonder-specific attributes.
           * In this case we extract a minimum wonder msg from the sip message.
           * We will use the Sip-CallId as context-ID for this call.
           */
          if (!wonderMsg) {
            // create a minumum wondermsg
            console.log("received non-wonder/pure SIP request: --> generating minimum wonder message with contextId: " + self.contextIDs[msg.attributes.sessionId]);
            wonderMsg = new Object();
            wonderMsg.id = guid();
            wonderMsg.contextId = self.contextIDs[msg.attributes.sessionId];
            wonderMsg.misc = new Object();
            wonderMsg.misc.connectionDescription = {
              sdp: msg.attributes.sdp,
              type: msg.attributes.callStatus
            }
            if (msg.attributes.callStatus == "offer") {
              wonderMsg.type = "invitation";
            } else if (msg.attributes.callStatus == "answer") {
              wonderMsg.type = "accepted";
              // send ACK (startwebtccallresponse) back and forward event to app
              var attributes = new Object();
              attributes.sessionId = msg.attributes.sessionId;
              attributes.info = "thanks";
              self.sendMessage(self.ims2CloudMessage("receivewebrtccallresponse", 10, attributes));
            }
          } else {
            self.sessionIDs[wonderMsg.conversationId] = msg.attributes.sessionId;
          }
          // let the lib handle the wonder-message
          self.onMessage(wonderMsg);
          break;
        case 'terminatesessionrequest': // incoming bye
          // send terminatesessionresponse back and forward event to app
          var attributes = new Object();
          attributes.sessionId = msg.attributes.sessionId; // ims-callid
          attributes.info = "deleting session";
          self.sendMessage(self.ims2CloudMessage("terminatesessionresponse", 10, attributes));
          // TODO: change this on messaging Server side, bye needs the wonder-body
          if (!wonderMsg) {
            wonderMsg = new Object();
            wonderMsg.type = MessageType.bye;
            wonderMsg.conversationId = "todo";
          }
          self.onMessage(wonderMsg);
          break;
        case 'callupdaterequest':
          // can be incoming "denied", "cancel", "ringing", "ConnectivityCandidate"
          // send callupdateresponse back and forward event to app
          self.updateReceived++;
          console.log("################# connectivityCandidates Received: " + self.updateReceived);

          var attributes = new Object();
          attributes.sessionId = msg.attributes.sessionId; // ims-callid
          self.sendMessage(self.ims2CloudMessage("callupdateresponse", 10, attributes));
          // TODO: invoke onMessage, if its a connectivity candidate
          if (wonderMsg) // should only be filled for connectivity candidates
            self.onMessage(wonderMsg);
          break;
        default:
          console.log("?????? received unknown message: " + msg);
          console.log("forwarding original message to listeners");
          self.onMessage(msg);
          break;
      }
    };


    disconnect() {
      console.log("disconnecting and closing socket...");
      if (this.socket)
        this.socket.close();
      this.socket = null;
      this.listeners = new Array(new Array(), new Array(), new Array());
      this.buffer = new Array();
    };

    doSend(message) {
      console.log("send message " + message.cmd);
      var msgString = JSON.stringify(message);
      this.socket.send(msgString);
    };

    ims2CloudMessage(message_command, status_code, attributes) {
      var msg = new Object();
      msg.cmd = message_command;
      msg.status = status_code;
      msg.attributes = attributes;
      return msg;
    };

  }
  return new MessagingStub_IMS_Local();
});

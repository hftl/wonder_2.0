var express = require('express');
var cors = require('cors');
var fs = require('fs');
var app = express();
var port;

// if first argument (Port) is set
if(process.argv[2]){
  port = process.argv[2]
} else port = 5000; // else use a standard port

//CORS middleware
var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
}
app.use(allowCrossDomain);

var users = function(req, res) {
    var callback;
    var domain;
    var user;
    var filePath;
    var fileData;

    console.log(new Date() + 'received request: ');
    console.log(req.query);

    // set callback function if jsonp is set in url
    if (req.query.jsonp){
      callback = req.query.jsonp;
    } else {
      res.statusCode = 416;
      res.send('No callback (jsonp) function was given');
    }
    // set user-uri if url contains it
    if (req.query.filter_rtcIdentity){
      if(req.query.filter_rtcIdentity === undefined) {
        res.statusCode = 416;
        res.send('Received Identity of \'undefined\' in filter_rtcIdentity');
      }
      // find corresponding file and read content from it
      var uri = req.query.filter_rtcIdentity;
      domain = uri.split('@')[1].toLowerCase();
      user = uri.split('@')[0];
      filePath = 'data/'+user+'_at_'+domain;

      fs.readFile(filePath, 'utf8', function (err, data) {
        if (err) { // try fallback to domain defaults
          filePath = 'data/'+domain;
          fs.readFile(filePath, function (err, data) {
            if (err) {
              res.statusCode = 404;
              res.send('Found no stub for '+user+' at the '+domain+' domain');
            } else {
              fileData = JSON.parse(data);
              returnFunction();
            }
          });
        } else {
          fileData = JSON.parse(data);
          returnFunction();
        }
      });
    } else {
      res.statusCode = 416;
      res.send('Received no Identity in filter_rtcIdentity');
    }

    // build callback function
    function returnFunction(){
      // check if callback exists
      if(callback){
        callback = callback+'('+JSON.stringify(fileData)+')';
        res.send(callback); // send callback function with data
      }
    }
};

// resources
app.get('/', function(req, res){
  res.send('please use the /users resource')
});
app.get('/users', users);

app.listen(port);
console.log((new Date()) + " Idp is listening on port ", port);

//on server side: npm install jsonwebtoken
var jwt = require('jsonwebtoken');
var express = require('express');
var cors = require('cors');
var fs = require('fs');

var app = express();
var port;

// if first argument (Port) is set
if(process.argv[2]){
  port = process.argv[2]
} else port = 5001; // else use a standard port

//CORS middleware
var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
}
app.use(allowCrossDomain);

var openid = function(req, res) {
    var callback;
    var domain;
    var user;
    var filePath;
    var fileData;


    console.log(new Date() + 'received request: ');
    console.log(req.query);

    // set callback function if jsonp is set in url
    if (req.query.jsonp){
      callback = req.query.jsonp;
    } else {
      res.statusCode = 416;
      res.send('No callback (jsonp) function was given');
    }
    // set user-uri if url contains it
    if (req.query.filter_idToken){
      if(req.query.filter_idToken === undefined) {
        res.statusCode = 416;
        res.send('Received undefined as idToken');
      }
      // get  and read content from it
      //console.log(cert);
      var decoded = jwt.decode(req.query.filter_idToken); //string
      console.log(decoded);
      if ( decoded.aud=="474418942796-8tvmp3cs5bf3mee0fgv9145cbova9c1o.apps.googleusercontent.com" &&
          (decoded.iss=="accounts.google.com" || decoded.iss=="https://accounts.google.com") &&
           decoded.exp>Math.floor(Date.now() / 1000)){
             console.log("check passed except we didn't check for the validity of the answer because googles public key didnt work with the verify function");
             fs.readFile("data/nodejs.wonder", function (err, data) {
                 if (err) {
                   res.statusCode = 404;
                   res.send('Found no stub at the nodejs.wonder domain');
                 } else {
                   fileData = JSON.parse(data);
                   returnFunction();
                 }
             });
      }

    } else {
      res.statusCode = 416;
      res.send('Received no idToken');
    }

    // build callback function
    function returnFunction(){
      // check if callback exists
      if(callback){
        callback = callback+'('+JSON.stringify(fileData)+')';
        res.send(callback); // send callback function with data
      }
    }
};

// resources
app.get('/', function(req, res){
  res.send('please use the /openid resource')
});
app.get('/openid', openid);

app.listen(port);
console.log((new Date()) + " Idp is listening on port ", port);

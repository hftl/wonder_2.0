# NodeJS Identity Provider (Idp)
This identity provider will return a function callback with an object including all information about a messaging-stub.

a response could be looked like that:
```
returnIdentity({
    "offset" : 0,
    "rows" : [{
      "messagingStubURL" : "http://127.0.0.1:80/stubs/Stub_a.js",
      "messagingServer" : "ws://127.0.0.1:1337"
    }],
    "total_rows" : 1,
    "millis" : 0
  })
```
## How to use
### Before you can start
* run `npm install` to install all dependent node-modules
* move the `data.example`-Folder to `data` or insert `json`-Snippets in this folder

### Starting the Idp
* you can start the Idp with `node server.js`
* also you can start it with `forever` or simply [create an init.d-script for that](https://github.com/d-koppenhagen/node-init.d-script-generator)

### Requesting an Identity
* you need to execute perform an HTTP `GET`-Request like that:
`GET http://localhost:5000/users/?filter_rtcIdentity=a@doman&jsonp=returnIdentity`

* parameters
  * `filter_rtcIdentity` contains the identity
  * `jsonp` contains the name of the function callback

# Simple Messaging Server
this repository provides a messaging server for multiparty communication.

## Setup
* you need to run `npm install` to install dependent packages

## Starting the server
* start the server with `node server.js` to run the server at port 1337 (default)
* start the server with `node server.js PORT` to run the server at a specific port

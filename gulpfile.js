'use strict';

var gulp = require('gulp');
var colors = require('colors');
var esdoc = require("gulp-esdoc");
var shell = require('gulp-shell');
var os = require('os');
var open = require('gulp-open');
var webserver = require('gulp-webserver');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var watch = require('gulp-watch');
var fs = require('fs');
var argv = require('yargs').argv;
var confirm = require('gulp-confirm');
var wrap = require('gulp-wrap-amd');


/* define default urls */
var defaultUri = {
  doc: {
    host: '0.0.0.0',
    port: '8088'
  },
  test: {
    host: '0.0.0.0',
    port: '8080',
    path: 'test/unit'
  }
}

/* set browser configuration for different os */
var browser = os.platform() === 'linux' ? 'google-chrome' : (
os.platform() === 'darwin' ? 'google chrome' : (
os.platform() === 'win32' ? 'chrome' : 'firefox'));



/* Setup */
gulp.task('setup', ['sampleData','certs'], shell.task([
  'cd src/idp ; npm install &',
  'cd src/simplems ; npm install &',
  'cd src/webfinger ; npm install &',
  'cd src/data-codec-provider ; npm install &',
  'cd src/stub-provider ; npm install'
]))



/* build */
gulp.task('build', ['concat'], function() {
  return gulp.src('src/libs/modules/*.js')
    .pipe(gulp.dest('src/dist'));
})
gulp.task('concat', function() {
  return gulp.src('src/libs/*.js')
    .pipe(sourcemaps.init())
    .pipe(concat('wonder.js'))
    /*
    .pipe(wrap({
      exports: 'wonder'         // variable to return
    }))
    */
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('src/dist'));
})
gulp.task('watch', function() {
    gulp.watch('src/libs/**/*.js', {readDelay: 250}, ['build']);
})



/* delete node modules */
gulp.task('clean', function(){
  return gulp.src('',{
      read: false
    }).pipe(confirm({
      // Static text.
      question: 'This will delete files, are you sure (y/n)?',
      input: '_key:y'
    }))
    .pipe(shell([
        'rm -r node_modules',
        'cd src/idp ; rm -r node_modules',
        'cd src/simplems ; rm -r node_modules',
        'cd src/webfinger ; rm -r node_modules',
        'cd src/stub-provider ; rm -r node_modules',
        'cd src/data-codec-provider ; rm -r node_modules'
      ])
    );
})



/* copy ignored files to orign if !exists */
gulp.task('sampleData', ['sampleData:idp', 'sampleData:webfinger'], function() {

})
gulp.task('sampleData:idp', function() {
  var path = 'src/idp/data';
  fs.exists(path, function (exists) {
    if (exists === false) {
      console.log("folder "+ path +" does not exist, creating this folder with example data...".green.bold);
      gulp.src(path+'.example/*')
        .pipe(gulp.dest(path));
    }
  });
})
gulp.task('sampleData:webfinger', function() {
  var webfingerpath = 'src/webfinger';
  var datapath = webfingerpath+'/data';
  var configfile = webfingerpath+'/config.js';

  fs.exists(datapath, function (exists) {
    if (exists === false) {
      console.log("folder "+ datapath +" does not exist, creating this folder with example data...".green.bold);
      gulp.src(datapath+'.example/**')
        .pipe(gulp.dest(datapath));
    }
  });
})

/* generate self-signed certificates */
gulp.task('certs', ['cert:webfinger', 'cert:https'], function() {

})
gulp.task('cert:webfinger', function() {
  var path = 'src/webfinger/certs';
  fs.exists(path, function (exists) {
    if (exists === false) {
      console.log("no certs in "+ path +", creating certs folder with certificates".green.bold);
      return gulp.src('src/webfinger',{
        read: false
      }).pipe(shell([
        'mkdir src/webfinger/certs',
        'openssl genrsa -out '+path+'/webfinger.key 2048',
        'openssl req -new -sha256 -nodes -subj \'/CN= \' -key '+path+'/webfinger.key -out '+path+'/webfinger.csr',
        'openssl x509 -req -in '+path+'/webfinger.csr -signkey '+path+'/webfinger.key -out '+path+'/webfinger.crt'
      ])
    );

    }
  });
})
gulp.task('cert:https', function() {
  var path = 'certs';
  fs.exists(path, function (exists) {
    if (exists === false) {
      console.log("no certs in "+ path +", creating certs folder with certificates".green.bold);
      return gulp.src('./',{
        read: false
      }).pipe(shell([
        'mkdir certs',
        'openssl genrsa -out '+path+'/webserver.key 2048',
        'openssl req -new -sha256 -nodes -subj \'/CN= \' -key '+path+'/webserver.key -out '+path+'/webserver.csr',
        'openssl x509 -req -in '+path+'/webserver.csr -signkey '+path+'/webserver.key -out '+path+'/webserver.crt'
      ])
    );

    }
  });
})



/* Default Gulp task (start all servers) */
gulp.task('default', ['build','idp','ms','webfinger','stub-provider','data-codec-provider','http','watch'], function() {

});
gulp.task('idp', shell.task([
  'cd src/idp ; node server.js &'
]))
gulp.task('ms', shell.task([
  'cd src/simplems ; node server.js &'
]))
gulp.task('webfinger', shell.task([
  'cd src/webfinger ; node server.js &'
]))
gulp.task('stub-provider', shell.task([
  'cd src/stub-provider ; node server.js &'
]))
gulp.task('data-codec-provider', shell.task([
  'cd src/data-codec-provider ; node server.js &'
]))
gulp.task('https', function() {
  gulp.src('src')
    .pipe(webserver({
      host: '0.0.0.0',
      port: '8080',
      livereload: false,
      open: 'apps/min',
      https: {
        key: 'certs/webserver.key',
        cert: 'certs/webserver.crt',
        csr: 'certs/webserver.csr'
      }
    }));
})
gulp.task('http', function() {
  gulp.src('src')
    .pipe(webserver({
      host: '0.0.0.0',
      port: '8080',
      livereload: false,
      open: 'apps/min'
    }));
})



/* Rebuild and show es-doc documentation */
gulp.task('doc', ['build','doc:build','doc:show'], function() {
});
gulp.task('doc:build', function() {
  // documentation: https://esdoc.org/config.html
  var options = {
    destination: "./docs",
    unexportIdentifier: true,
    excludes: ["\\adapter\\.(js|es6)$"]
  }

  gulp.src("./src/libs")
    .pipe(esdoc(options));
});
gulp.task('doc:show', function() {

    gulp.src('docs')
        .pipe(webserver({
          livereload: true,
          host: argv.host || defaultUri.doc.host,
          port: argv.port || defaultUri.doc.port,
          open: true
        }));
});



/* Tests */
gulp.task('test', ['default','test:unit', 'test:int']);

/* unit tests with qunit */
gulp.task('test:unit', function() {
  var host = argv.host || defaultUri.test.host;
  var port = argv.port || defaultUri.test.port;
  var path = argv.path || defaultUri.test.path;

  var options = {
    uri: 'http://'+ host + ':'+ port + '/' + path,
    app: browser
  };

  gulp.src(__filename)
    .pipe(open(options));
});

/* integration tests with qunit */
gulp.task('test:int', function() {
  console.log("this isn't implemented yet.");
});



/* show man */
gulp.task('help', function() {
  console.log('\nThe following gulp tasks are available:\n'.bold.cyan);
  console.log('gulp' + '\t\t\t'+'# Start all servers, build and concat files, etc.'.white);
  console.log('gulp' + ' ' + 'idp'.green.bold + '\t\t' + '# Start the Identity Provider'.white);
  console.log('gulp' + ' ' + 'webfinger'.green.bold + '\t\t' + '# Start the Webfinger Idp'.white);
  console.log('gulp' + ' ' + 'stub-provider'.green.bold + '\t' + '# Start the Messaging Stub Provider'.white);
  console.log('gulp' + ' ' + 'data-codec-provider'.green.bold + '' + '# Start the Data Codec Provider'.white);
  console.log('gulp' + ' ' + 'ms'.green.bold + '\t\t\t' + '# Start the MessagingServer'.white);
  console.log('gulp' + ' ' + 'http'.green.bold + '\t\t' + '# Start the Webserver (http)'.white);
  console.log('gulp' + ' ' + 'https'.green.bold + '\t\t' + '# Start the Webserver (https)'.white);
  console.log('\t\t\t\t\t\t\t\t\t\t\t\t'.underline.magenta);
  console.log('gulp' + ' ' + 'setup'.green.bold + '\t\t' + '# Setup (install npm packages for servers, sample data and certificates)'.white);
  console.log('\t\t\t\t\t\t\t\t\t\t\t\t'.underline.magenta);
  console.log('gulp' + ' ' + 'build'.green.bold + '\t\t' + '# This builds the wonder framework (single file)'.white);
  console.log('\t\t\t\t\t\t\t\t\t\t\t\t'.underline.magenta);
  console.log('gulp' + ' ' + 'doc'.green.bold + '\t\t' + '# Build the docs an start the Webserver for docs'.white);
  console.log('gulp' + ' ' + 'doc:build'.green.bold + '\t\t' + '# Build the docs'.white);
  console.log('gulp' + ' ' + 'doc:show'.green.bold + '\t\t' + '# Start the docs Webserver and opens the browser'.white);
  console.log('options:'.yellow + '');
  console.log('   --host=HOST'.green.bold + '\t\t' + '# use different host (default:'.white+ defaultUri.doc.port.white+')'.white);
  console.log('   --port=PORT'.green.bold + '\t\t' + '# use different port (default:'.white+ defaultUri.doc.port.white+')'.white);
  console.log('\t\t\t\t\t\t\t\t\t\t\t\t'.underline.magenta);
  /*
  console.log('gulp' + ' ' + 'test'.green.bold + '\t\t' + '# Run all tests'.white);
  console.log('gulp' + ' ' + 'test:unit'.green.bold + '\t\t' + '# Run unit tests'.white);
  console.log('gulp' + ' ' + 'test:int'.green.bold + '\t\t' + '# Run integration tests'.white);
  console.log('gulp' + ' ' + 'test:ui'.green.bold + '\t\t' + '# Run UI tests'.white);
  console.log('options:'.yellow + '');
  console.log('   --host=HOST'.green.bold + '\t\t' + '# use different host (default:'.white+ defaultUri.test.port.white+')'.white);
  console.log('   --port=PORT'.green.bold + '\t\t' + '# use different port (default:'.white+ defaultUri.test.port.white+')'.white);
  console.log('   --path=PATH'.green.bold + '\t\t' + '# use different path (default:'.white+ defaultUri.test.path.white+')'.white);
  console.log('\t\t\t\t\t\t\t\t\t\t\t\t'.underline.magenta);
  */
  console.log('gulp' + ' ' + 'help'.green.bold + '\t\t' + '# Show this dialog'.white);
  console.log('\t\t\t\t\t\t\t\t\t\t\t\t'.underline.magenta);
  console.log('gulp' + ' ' + 'clean'.green.bold + '\t\t' + '# Delete all node_modules directories, and sample data'.white);
  console.log('\t\t\t\t\t\t\t\t\t\t\t\t'.underline.magenta);
  console.log('gulp' + ' ' + 'certs'.green.bold + '\t\t' + '# create certificates'.white);
  console.log('gulp' + ' ' + 'cert:https'.green.bold + '\t\t' + '# create certificate for webserver'.white);
  console.log('gulp' + ' ' + 'cert:webfinger'.green.bold + '\t' + '# create certificate for webfinger'.white);
  console.log('\t\t\t\t\t\t\t\t\t\t\t\t'.underline.magenta);
});
